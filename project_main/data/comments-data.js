import pool from './pool.js';

const getCommentsBy = async (column, value) => {
    const sql = `
        SELECT c.id, c.content, c.userId, c.postId, c.isDeleted, c.createdOn, u.username, u.nickname
        FROM comments c
        JOIN users u on u.id = c.userId
        WHERE ${column} = ?
        AND c.isDeleted = 0;
    `;

    const result = await pool.query(sql, [value]);

    return result[0];
};

const createCommentEntry = async (content, userId, postId) => {
    const sql = `
        INSERT INTO comments (content, userId, postId)
        VALUES (?,?,?)
    `;
    const result = await pool.query(sql, [content, userId, postId]);

    return {
        id: result.insertId,
        content: content,
        userId: userId,
        postId: postId,
    };
};

const removeCommentEntry = async (id) => {
    const sql = `
        UPDATE comments SET
        isDeleted = 1
        WHERE id = ?
    `;
    const result = await pool.query(sql, [id]);
    return result;

};

const updateCommentEntry = async (id, value) => {
    const sql = `
        UPDATE comments 
        SET content = ?
        WHERE id = ?
    `;
    const result = await pool.query(sql, [value, id]);
    return result;
};

const getAllComments = async (column, value) => {
    const sql = `
        SELECT c.id, c.content, c.userId, c.postId, c.createdOn, c.isDeleted, count(cl.userId) as likes, u.username, u.nickname
        FROM comments c
        LEFT JOIN comment_likes cl on c.id = cl.commentId
        JOIN users u on u.id = c.userId
        WHERE ${column} = ?
        AND c.isDeleted = 0
        group by c.id;
    `;
 
    const result = await pool.query(sql, [value]);
 
    return result;
};

const createCommentLike = async (commentId, userId) => {
    const sql = `
        INSERT INTO comment_likes(commentId, userId)
        VALUES(?, ?)

    `;
    return await pool.query(sql, [commentId, userId]);
};

const getAllCommentLikes = async (commentId) => {
    const sql = `
        SELECT *
        FROM comment_likes
        WHERE commentId = ?
    `;
    const result = await pool.query(sql, [commentId]);
    return result.length;
};

const userLikedComments = async (userId) => {
    const sql =`
        SELECT *
        FROM comment_likes as cl
        JOIN users u on u.id = cl.userId
        WHERE userId = ?
    `;
    return await pool.query(sql, [userId]);
};

export default {
    getCommentsBy,
    createCommentEntry,
    removeCommentEntry,
    updateCommentEntry,
    getAllComments,
    createCommentLike,
    getAllCommentLikes,
    userLikedComments,
};