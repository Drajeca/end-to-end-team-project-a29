import pool from './pool.js';


const getAllPosts = async () => {
    const sql = `
        SELECT p.id, p.title, p.content, p.userId, p.createdOn, p.isLocked, count(pl.userId) as likes, u.username, u.nickname
        FROM posts p
        LEFT JOIN post_likes pl on p.id = pl.postId
        JOIN users u on u.id = p.userId
        WHERE p.isDeleted = 0
        GROUP BY p.id
    `;
 
    const result = await pool.query(sql);
 
    return result;
};

const getAllPostsBy = async (column, value) => {
    const sql = `
        SELECT p.id, p.title, p.content, p.userId, p.createdOn, p.isLocked, count(pl.userId) as likes, u.username, u.nickname
        FROM posts p
        LEFT JOIN post_likes pl on p.id = pl.postId
        JOIN users u on u.id = p.userId
        WHERE p.isDeleted = 0
        AND ${column} = ?
        group by p.id
    `;
 
    const result = await pool.query(sql, [value]);
 
    return result;
};
 
const searchBy = async (column, value) => {
    const sql = `
        SELECT title, content, userId, createdOn
        FROM posts
        WHERE ${column} LIKE '%${value}%'
        AND isDeleted = 0; 
    `;
 
    return await pool.query(sql);
};
 
const getPostBy = async (column, value) => {
    const sql = `
        SELECT p.id, p.title, p.content, p.userId, p.createdOn, p.isLocked, count(pl.userId) as likes, u.username, u.nickname
        FROM posts p
        LEFT JOIN post_likes pl on p.id = pl.postId
        JOIN users u on u.id = p.userId
        WHERE p.isDeleted = 0
        AND ${column} = ?
        group by p.id

    `;
 
    const result = await pool.query(sql, [value]);
 
    return result[0];
};

const createPostEntry = async (title, content, userId) => {
    const sql = `
        INSERT INTO posts (title, content, userId)
        VALUES (?,?,(SELECT id FROM users WHERE username = ?))
    `;
    const result = await pool.query(sql, [title, content, userId]);

    return {
        id: result.insertId,
        title: title,
        content: content,
    };
};

const updatePostEntry = async (id, value_1, value_2) => {
    const sql = `
        UPDATE posts SET
        title = ?,
        content = ?
        WHERE id = ?
    `;
    const result = await pool.query(sql, [value_1, value_2, id]);
    return result;
};


const removePostEntry = async (id) => {
    const sql = `
        UPDATE posts SET
        isDeleted = 1
        WHERE id = ?
    `;
    const result = await pool.query(sql, [id]);
    return result;

};

const createPostLike = async (userId, postId) => {
    const sql = `
        INSERT INTO post_likes(postId, userId)
        VALUES(?, ?)

    `;
    return await pool.query(sql, [userId, postId]);
};

const getAllPostLikes = async (postId) => {
    const sql = `
        SELECT *
        FROM post_likes
        WHERE postId = ?
    `;
    const result = await pool.query(sql, [postId]);
    return result.length;
};

const lockPostEntry = async (lockStatus, postId) => {
    const sql = `
        UPDATE posts
        SET isLocked = ?
        WHERE id = ?
    `;

    return await pool.query(sql, [lockStatus, postId]);
};

const userLikedPosts = async (userId) => {
    const sql =`
        SELECT *
        FROM post_likes as pl
        JOIN users u on u.id = pl.userId
        WHERE userId = ?
    `;
    return await pool.query(sql, [userId]);
};

export default {
    getAllPosts,
    searchBy,
    getPostBy,
    getAllPostsBy,
    createPostEntry,
    updatePostEntry,
    removePostEntry,
    createPostLike,
    getAllPostLikes,
    lockPostEntry,
    userLikedPosts,
};