import pool from './pool.js';

const getAll = async () => {
    const sql = `
        SELECT id, username, nickname, createdOn, roleId, isBanned
        FROM users
        WHERE isDeleted = 0
    `;

    return await pool.query(sql);
};

const searchBy = async (column, value) => {
    const sql = `
        SELECT id, username, nickname, createdOn, roleId, isBanned
        FROM users
        WHERE ${column} LIKE '%${value}%'
        AND isDeleted = 0 
    `;

    return await pool.query(sql);
};

const create = async (username, password, nickname, role) => {
    const sql = `
        INSERT INTO users(username, password, nickname, roleId)
        VALUES (?,?,?,(SELECT roleId FROM roles WHERE name = ?))
    `;

    const result = await pool.query(sql, [username, password, nickname, role]);

    return {
        id: result.insertId,
        username: username,
    };
};

const remove = async (id) => {
    const sql = `
        UPDATE users 
        SET isDeleted = 1
        WHERE id = ?
    `;

    return await pool.query(sql, [id]);
};

const getBy = async (column, value) => {
    const sql = `
        SELECT id, username, nickname, createdOn, isBanned
        FROM users
        WHERE ${column} = ?
    `;

    const result = await pool.query(sql, [value]);

    return result[0];
};

const getWithRole = async (username) => {
    const sql = `
        SELECT u.id, u.username, u.password, u.isBanned, r.name as role
        FROM users u
        JOIN roles r ON u.roleId = r.roleId
        WHERE u.username = ?
    `;

    const result = await pool.query(sql, [username]);

    return result[0];
};

const ban = async (id, dateUntil) => {
    const sql = `
      UPDATE users 
      SET isBanned = ?
      WHERE id = ?
    `;
  
    return await pool.query(sql, [dateUntil, id]);
  };

export default {
    getAll,
    searchBy,
    create,
    remove,
    getBy,
    getWithRole,
    ban,
};

