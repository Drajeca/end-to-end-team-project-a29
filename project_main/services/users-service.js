import serviceErrors from './service-errors.js';
import { ADMIN_USER_ROLE, DEFAULT_USER_ROLE } from './../config.js';
import bcrypt from 'bcrypt';

const signInUser = usersData => {
    return async (username, password) => {
        const user = await usersData.getWithRole(username);

        if (!user || !(await bcrypt.compare(password, user.password))) {
            return {
                error: serviceErrors.INVALID_SIGNIN,
                user: null,
            };
        }

        else if (user.isBanned !== null && (user.isBanned > new Date())) {
            return {
                error: serviceErrors.USER_BANNED,
                user: user,
            };
        }

        return {
            error: null,
            user: user,
        };
    };
};

const getAllUsers = usersData => {
    return async (filter) => {
        return filter
            ? await usersData.searchBy('username', filter)
            : await usersData.getAll();
    };
};

const createUser = usersData => {
    return async (userCreate) => {
        const { username, password, nickname } = userCreate;

        const existingUsername = await usersData.getBy('username', username);
        const existingNickname = await usersData.getBy('nickname', nickname);

            if (existingUsername?.username === username || existingNickname?.nickname === nickname) {
                return {
                    error: serviceErrors.DUPLICATE_RECORD,
                    user: null,
                };
            }

        const passwordHash = await bcrypt.hash(password, 10);
        const user = await usersData.create(username, passwordHash, nickname, DEFAULT_USER_ROLE);

        return { error: null, user: user };
    }; 
};

const deleteUser = usersData => {
    return async (id, userId, role) => {
        const userToDelete = await usersData.getBy('id', id);
        if (!userToDelete) {
            return {
                error: serviceErrors.RECORD_NOT_FOUND,
                user: null,
            };
        }
        if(role !== ADMIN_USER_ROLE ){
            return {
                error: serviceErrors.OPERATION_NOT_PERMITTED,
                user: null, 
            };
        }

        /*eslint no-unused-vars: "off"*/
        const _ = await usersData.remove(id);
 
        return {
            error: null, 
            user: userToDelete,
        };
    };
};

const getUserById = usersData => {
    return async (id) => {
        const user = await usersData.getBy('id', id);

        if (!user) {
            return {
                error: serviceErrors.RECORD_NOT_FOUND,
                user: null,
            };
        }

        return { error: null, user: user };
    };
};

const banUser = usersData => {
    return async (id, durationInDays) => {
        // const durationMs = durationInDays * 24 * 60 * 60 * 1000;
        const bannedUntilDate = new Date(durationInDays);
        // bannedUntilDate.setMilliseconds(bannedUntilDate.getMilliseconds() + durationMs);
  
        const banUser = await usersData.ban(id, bannedUntilDate);
        const result = await usersData.getBy('id', id);

        if (!banUser.affectedRows) {
            return {
                error: serviceErrors.BAN_FAILED,
                user: null,
            };
        }

        if(!result) {
            return {
                error: serviceErrors.RECORD_NOT_FOUND,
                user: null,
            };
        }

        return {
            error: null,
            user: result,
        };
    };
  };

export default {
    signInUser,
    getAllUsers,
    createUser,
    deleteUser,
    getUserById,
    banUser,
};

