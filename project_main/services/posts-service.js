import { ADMIN_USER_ROLE, DEFAULT_USER_ROLE } from '../config.js';
import serviceErrors from './service-errors.js';

const readPosts = postData => {
    return async (filter) => {
      if(filter) {
          const filteredPost = await postData.searchBy('title', filter);
          if(filteredPost.length) {
              return {
                  error: null,
                  post: filteredPost,
              };
          }
          else{
          return {
              error: serviceErrors.RECORD_NOT_FOUND,
              post: null,
          };
        }
      }
      const allPosts = await postData.getAllPosts();
      if(allPosts) {
          return {
              error: null,
              post: allPosts,
          };
      }
      return {
          error: serviceErrors.RECORD_NOT_FOUND,
          post: null,
      };
    };
  };

  const readAllUserPosts = postData => {
    return async (userId) => {
      const allPosts = await postData.getAllPostsBy('p.userId', userId);
      if(allPosts) {
          return {
              error: null,
              post: allPosts,
          };
      }
      return {
          error: serviceErrors.RECORD_NOT_FOUND,
          post: null,
      };
    };
  };
 
  const readIndividualPost = (postData) => {
      return async (id) => {
          const foundPost =  await postData.getPostBy('p.id', id);
          if(foundPost) {
              return {
                  error: null,
                  post: foundPost,
              };
          }
            return {
                error: serviceErrors.RECORD_NOT_FOUND,
                post: null,
            };
      };
  };

const createNewPost = postData => {
    return async (postToCreate) => {
        const { title, content, userId } = postToCreate;

        const existingPost = await postData.getPostBy('p.title', title);

        if (existingPost) {
            return {
                error: serviceErrors.DUPLICATE_RECORD,
                post: null,
            };
        }

        const post = await postData.createPostEntry(title, content, userId);

        return { error: null, post: post };
    };  
};

const updatePost = postData => {
    return async (id, role, userId, title, content) => {
        const postToUpdate = await postData.getPostBy('p.id', id);
 
        if(!postToUpdate) {
            return {
                error: serviceErrors.RECORD_NOT_FOUND,
                post: null,
            };
        }
 
        if(role === DEFAULT_USER_ROLE) {
            if(userId !== postToUpdate.userId) {
                return {
                    error: serviceErrors.OPERATION_NOT_PERMITTED,
                    post: null,
                };
            }
 
        }
        /*eslint no-unused-vars: "off"*/
        const _ = await postData.updatePostEntry(id, title, content);
        const result = await postData.getPostBy('p.id', id);
 
        return { error: null, post: result};
    };
};

const deletePost = postData => {
    return async (id, role, userId) => {
        const postToDelete = await postData.getPostBy('p.id', id);

        if(!postToDelete) {
            return {
                error: serviceErrors.RECORD_NOT_FOUND,
                post: null,
            };
        }

        if(role === DEFAULT_USER_ROLE) {
            if(userId !== postToDelete.userId) {
                return {
                    error: serviceErrors.OPERATION_NOT_PERMITTED,
                    post: null,
                };
            }

        }
        /*eslint no-unused-vars: "off"*/
        const _ = await postData.removePostEntry(id);

        return { error: null, post: postToDelete };
    };
};

const likePost = postData => {
    return async (postId, userId) => {
        const postToLike = await postData.getPostBy('p.id', postId);

        if(!postToLike) {
            return {
                error: serviceErrors.RECORD_NOT_FOUND,
                post: null,
            };
        }

            /*eslint no-unused-vars: "off"*/
            try {
                const _ = await postData.createPostLike(postId, userId);
            } catch (e) {
                return {
                    error: serviceErrors.DUPLICATE_RECORD,
                    post: null,
                };
            }
            

            const likes = await postData.getAllPostLikes(postId);

            return { error: null, post: {...postToLike, likes }};
    };
};

const getAllLikes = postData => {
    return async (postId) => {
        const postToLike = await postData.getPostBy('p.id', postId);

        if(!postToLike) {
            return {
                error: serviceErrors.RECORD_NOT_FOUND,
                post: null,
            };
        }
        const likes = await postData.getAllPostLikes(postId);

        return { error: null, post: { likes } };
    };
};

const lockPost = postData => {
    return async (id, role, lockStatus ) => {
        const postToLock = await postData.getPostBy('p.id', id);

        if(!postToLock) {
            return {
                error: serviceErrors.RECORD_NOT_FOUND,
                post: null,
            };
        }

        if(role !== ADMIN_USER_ROLE) {
            return {
                error: serviceErrors.OPERATION_NOT_PERMITTED,
                post: null,
            };
        }
        /*eslint no-unused-vars: "off"*/
        const _ = await postData.lockPostEntry(lockStatus, id);
        const result = await postData.getPostBy('p.id', id);

        return { error: null, post: result };
    };
};

const AllPostsLikedByTheUser = postData => {
    return async (userId) => {
        return await postData.userLikedPosts(userId);
    };
};

export default {
    readPosts,
    readIndividualPost,
    readAllUserPosts,
    createNewPost,
    updatePost,
    deletePost,
    likePost,
    getAllLikes,
    lockPost,
    AllPostsLikedByTheUser,
};