import serviceErrors from './service-errors.js';
import postData from './../data/posts-data.js';
import { DEFAULT_USER_ROLE } from '../config.js';

const createNewComment = commentData => {
    return async (commentToCreate) => {
        const { content, userId, postId } = commentToCreate;

        const comment = await commentData.createCommentEntry(content, userId, postId);

        return { comment: comment };
    };  
};

export const deleteComment = commentData => 
    async (postId, commentId, userId, role) => {
    // if a post with that id does not exist, return an error
        const currentPost = await postData.getPostBy('p.id', postId);

        if (!currentPost) {
            return {
                error: serviceErrors.RECORD_NOT_FOUND,
                comment: null,
            };
        }
    // if a comment with that id does not exist, return an error
    const currentComment = await commentData.getCommentsBy('c.id', commentId);
        if (!currentComment) {
            return {
                error: serviceErrors.RECORD_NOT_FOUND,
                comment: null,
            };
        }
    // obtain user data to check for update privileges
    // const currentUser = await getUserById(userId);
        if(role === DEFAULT_USER_ROLE) {
            if (currentComment.userId !== userId) {
                return {
                    error: serviceErrors.OPERATION_NOT_PERMITTED,
                    comment: null,
                };
            }
        }    
    // send the CommentId to the query to mark the is_deleted field as true
    /*eslint no-unused-vars: "off"*/
    const _ = await commentData.removeCommentEntry(commentId);
    return {
        error: null,
        comment: currentComment,
    };
  };

    const updateComment = commentData => 
    async (postId, commentId, userId, role, content) => {
    // if a post with that id does not exist, return an error
        const currentPost = await postData.getPostBy('p.id', postId);

        if (!currentPost) {
            return {
                error: serviceErrors.RECORD_NOT_FOUND,
                comment: null,
            };
        }
    // if a comment with that id does not exist, return an error
    const currentComment = await commentData.getCommentsBy('c.id', commentId);
        if (!currentComment) {
            return {
                error: serviceErrors.RECORD_NOT_FOUND,
                comment: null,
            };
        }

    // check user data for update privileges
        if(role === DEFAULT_USER_ROLE) {
            if (currentComment.userId !== userId) {
                return {
                    error: serviceErrors.OPERATION_NOT_PERMITTED,
                    comment: null,
                };
            }
        }    
    // send the CommentId to the query to mark the is_deleted field as true
    /*eslint no-unused-vars: "off"*/
    const _ = await commentData.updateCommentEntry(commentId, content);
    const result = await commentData.getCommentsBy('c.id', commentId);
    return {
        error: null,
        comment: result,
    };
  };

  const readComments = commentData => {
    return async (postId) => {
        const existingPost = await postData.getPostBy('p.id', postId);
 
        if(!existingPost) {
            return {
                error: serviceErrors.RECORD_NOT_FOUND,
                comment: null,
            };
        }
 
        const allPostComments = await commentData.getAllComments('c.postId', postId);
        if(!allPostComments.length) {
            return {
                error: serviceErrors.RECORD_NOT_FOUND,
                comment: null,
            };
        }
 
        return {
            error: null,
            comment: allPostComments,
        };
    };
};

const readIndividualComment = commentData => {
    return async (postId, commentId) => {
        const existingPost = await postData.getPostBy('p.id', postId);
 
        if(!existingPost) {
            return {
                error: serviceErrors.RECORD_NOT_FOUND,
                comment: null,
            };
        }
 
        const existingComment= await commentData.getCommentsBy('c.id', commentId);
        if(!existingComment) {
            return {
                error: serviceErrors.RECORD_NOT_FOUND,
                comment: null,
            };
        }
 
        return {
            error: null,
            comment: existingComment,
        };
    };
};

const likeComment = commentData => {
    return async (postId, commentId, userId) => {
        const existingPost = await postData.getPostBy('p.id', postId);

        if(!existingPost) {
            return {
                error: serviceErrors.RECORD_NOT_FOUND,
                comment: null,
            };
        }

        const commentToLike = await commentData.getCommentsBy('c.id', commentId);

        if (!commentToLike) {
            return {
                error: serviceErrors.RECORD_NOT_FOUND,
                comment: null,
            };
        }


            /*eslint no-unused-vars: "off"*/
            try {
                const _ = await commentData.createCommentLike(commentId, userId);
            } catch (e) {
                return {
                    error: serviceErrors.DUPLICATE_RECORD,
                    comment: null,
                };
            }
            

            const likes = await commentData.getAllCommentLikes(commentId);

            return { error: null, comment: {...commentToLike, likes }};
    };
};

const AllCommentsLikedByTheUser = commentData => {
    return async (userId) => {
        return await commentData.userLikedComments(userId);
    };
};

export default {
    createNewComment,
    deleteComment,
    updateComment,
    readComments,
    readIndividualComment,
    likeComment,
    AllCommentsLikedByTheUser,
};