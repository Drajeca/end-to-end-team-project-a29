export const createPostSchema = {
    title: value => {
        if (!value) {
            return 'Title is required';
        }
        
        if (typeof value !== 'string' || value.length < 1 || value.length > 200) {
            return 'The title should be a string in range [1..200]';
        }

        return null;
    },
    
    content: value => {
        if (!value) {
            return 'Content is required';
        }
        
        if (typeof value !== 'string' || value.length < 1 || value.length > 2000) {
            return 'The content should be a string in range [1..2000]';
        }

        return null;
    },
};