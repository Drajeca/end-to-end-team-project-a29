export const banUserSchema = {
    duration: value => {
      if (!value || (typeof value !== 'number') || value < 1 || value > 2 * 365) {
        return `duration must be a number between 1 day and 2 years (${2 * 365}) days`;
      }
  
      return null;
    },
  };