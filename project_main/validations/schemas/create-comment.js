export const createCommentSchema = {

    content: value => {
        if (!value) {
            return { message: 'Content is required' };
        }
        
        if (typeof value !== 'string' || value.length < 1 || value.length > 2000) {
            return { message: 'Content should be a string in range [1..2000]' };
        }

        return null;
    },
};