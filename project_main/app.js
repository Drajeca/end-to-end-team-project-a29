import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import { PORT } from './config.js';
import usersController from './controllers/user-controller.js';
import postsController from './controllers/post-controller.js';
import commentsController from './controllers/comment-controller.js';
import passport from 'passport';
import jwtStrategy from './auth/strategy.js';
import authController  from './controllers/auth-controller.js';
import adminController from './controllers/admin-controller.js';

const app = express();

passport.use(jwtStrategy);

app.use(cors(), bodyParser.json());
app.use(passport.initialize());
app.use('/users', usersController);
app.use('/auth', authController);
app.use('/posts', postsController);
app.use('/posts', commentsController);
app.use('/admin', adminController);

app.all('*', (req, res) =>
  res.status(404).send({ message: 'Resource not found!' }));

app.listen(PORT, () => console.log(`Listening on port ${PORT}`));
