import express from 'express';
import usersData from '../data/users-data.js';
import serviceErrors from '../services/service-errors.js';
import usersService from '../services/users-service.js';
import { createValidator, createUserSchema } from '../validations/index.js';
import { authMiddleware, roleMiddleware } from '../auth/auth-middleware.js';
import { ADMIN_USER_ROLE, DEFAULT_USER_ROLE } from '../config.js';

const usersController = express.Router();

usersController
    // get all users with searching
    .get('/', async (req, res) => {
        const { search } = req.query;
        const users = await usersService.getAllUsers(usersData)(search);

        res.status(200).send(users);
    })

    //get user by id
    .get('/:id',
    authMiddleware,
    roleMiddleware([DEFAULT_USER_ROLE, ADMIN_USER_ROLE]),
    async (req, res) => {
        const { id } = req.params;

        const { error, user } = await usersService.getUserById(usersData)(+id);

        if (error === serviceErrors.RECORD_NOT_FOUND) {
            res.status(404).send({ message: 'User not found!' });
        } else {
            res.status(200).send(user);
        }
    })

    // create user
    .post('/',
    createValidator(createUserSchema),
    async (req, res) => {
        const createData = req.body;

        const { error, user } = await usersService.createUser(usersData)(createData);
        if (error === serviceErrors.DUPLICATE_RECORD) {
             res.status(409).send({ message: 'Name not available' });
        } else {
             res.status(201).send(user);
        }
    });

    export default usersController;