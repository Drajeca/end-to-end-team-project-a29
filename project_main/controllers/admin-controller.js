import express from 'express';
import usersService from '../services/users-service.js';
import usersData from '../data/users-data.js';
import { authMiddleware, roleMiddleware } from '../auth/auth-middleware.js';
// import { createValidator, banUserSchema } from '../validations/index.js';
import { ADMIN_USER_ROLE } from '../config.js';
import serviceErrors from '../services/service-errors.js';
import postsService from '../services/posts-service.js';
import postData from './../data/posts-data.js';

const adminController = express.Router();

adminController.use(authMiddleware);
adminController.use(roleMiddleware(ADMIN_USER_ROLE));


adminController

// admin/ban user
.put('/ban/:id', /*createValidator(banUserSchema),*/ 
async (req, res) => {
  const { duration } = req.body;

  const { error, user } = await usersService.banUser(usersData)(+(req.params.id), duration);

  if(error === serviceErrors.RECORD_NOT_FOUND) {
    return res.status(404).send( {message: 'User not found!'});
  }
  if (error === serviceErrors.BAN_FAILED) {
    return res.status(400).send({ message: 'Ban failed'});
  }

  return res.status(201).send(user);
  
})

  // delete user by id
  .delete('/delete/:id',
  async (req, res) => {
      const { id } = req.params;
      const userId = req.user.id;
      const role = req.user.role;
      const { error, user } = await usersService.deleteUser(usersData)(+id, userId, role);
 
      if (error === serviceErrors.RECORD_NOT_FOUND) {
          res.status(404).send({ message: 'User not found!' });
      } else if (error === serviceErrors.OPERATION_NOT_PERMITTED) {
          res.status(403).send({ message: 'Only users with admin privileges can do that!' });
      }
      else {
          res.status(200).send(user);
      }
  })


      // lock post
      .put('/lock/:id',

      async (req, res) => {
          const { id } = req.params;
          const role = req.user.role;
          const { lockStatus } = req.body;

          const { error, post } = await postsService.lockPost(postData)(+id, role, lockStatus);
          
          if (error === serviceErrors.RECORD_NOT_FOUND) {
             return res.status(404).send({ message: 'Post not found!' });
          }
          
          if (error === serviceErrors.OPERATION_NOT_PERMITTED) {
              return res.status(403).send({ message: 'You need to have admin privileges to do that!' });
          }
              res.status(201).send(post);
      });
    export default adminController;