import express from 'express';
import postData from './../data/posts-data.js';
import { createValidator, createPostSchema } from '../validations/index.js';
import serviceErrors from '../services/service-errors.js';
import postsService from '../services/posts-service.js';
import { authMiddleware, roleMiddleware } from '../auth/auth-middleware.js';
import { ADMIN_USER_ROLE, DEFAULT_USER_ROLE } from '../config.js';

const postsController = express.Router();

postsController

//read all posts or search them by title
.get('/', async (req, res) => {
    const { title } = req.query;
    const { error, post } = await postsService.readPosts(postData)(title);
    if (error === serviceErrors.RECORD_NOT_FOUND) {
        res.status(409).send({ message: 'Post not found!' });
   } else {
        res.status(200).send(post);
   }
})

//read individual post by passing an id
.get('/:id', async (req, res) => {
    const { id } = req.params;
    const { error, post } = await postsService.readIndividualPost(postData)(+id);
    if (error === serviceErrors.RECORD_NOT_FOUND) {
        res.status(409).send({ message: 'Post not found!' });
   } else {
        res.status(200).send(post);
   }
})

    // create post
    .post('/',    
    authMiddleware,
    roleMiddleware([DEFAULT_USER_ROLE, ADMIN_USER_ROLE]),
    createValidator(createPostSchema),
    async (req, res) => {
        const createData = req.body;
        createData.userId = req.user.username;

        const { error, post } = await postsService.createNewPost(postData)(createData);
        if (error === serviceErrors.DUPLICATE_RECORD) {
             res.status(409).send({ message: 'Title not available' });
        } else {
             res.status(201).send(post);
        }
    })

    // update post
    .put('/:id',
    authMiddleware,
    roleMiddleware([DEFAULT_USER_ROLE, ADMIN_USER_ROLE]),
    async (req, res) => {
        const { id } = req.params;
 
        const role = req.user.role;
        const userId = req.user.id;
        const title = req.body.title;
        const content = req.body.content;
        const { error, post } = await postsService.updatePost(postData)(+id, role, userId, title, content);
 
        if (error === serviceErrors.RECORD_NOT_FOUND) {
           return res.status(404).send({ message: 'Post not found!' });
        }
 
        if(error === serviceErrors.OPERATION_NOT_PERMITTED) {
            return res.status(400).send({ message: 'You are not allowed to update other\'s people posts!' });
        }
            res.status(200).send(post);
    })

    // delete post
    .delete('/:id',
    authMiddleware,
    roleMiddleware([DEFAULT_USER_ROLE, ADMIN_USER_ROLE]),
    async (req, res) => {
        const { id } = req.params;

        const role = req.user.role;
        const userId = req.user.id;
        const { error, post } = await postsService.deletePost(postData)(+id, role, userId);
        
        if (error === serviceErrors.RECORD_NOT_FOUND) {
           return res.status(404).send({ message: 'Post not found!' });
        }
        
        if(error === serviceErrors.OPERATION_NOT_PERMITTED) {
            return res.status(400).send({ message: 'You are not allowed to delete other\'s people posts!' });
        }
            res.status(201).send(post);
    })

    //like post
    .post('/:id/likes',
    authMiddleware,
    roleMiddleware([DEFAULT_USER_ROLE, ADMIN_USER_ROLE]),
    async (req, res) => {
        const { id } = req.params;
        const userId = req.user.id;

        const { error, post } = await postsService.likePost(postData)(+id, userId);
        
        if(error === serviceErrors.RECORD_NOT_FOUND) {
            return res.status(404).send({ message: 'You are trying to like a post that doesn\'t exist!' });
        }

        if(error === serviceErrors.DUPLICATE_RECORD) {
            return res.status(409).send({ message: 'You already liked that post!'});
        }
            res.status(200).send(post);
    })

    //get all post likes
    .get('/:id/likes', async (req, res) => {
        const { id }  = req.params;
        const { error, post } = await postsService.getAllLikes(postData)(+id);
        if (error === serviceErrors.RECORD_NOT_FOUND) {
            return res.status(404).send({ message: 'Post not found!' });
        }    
            res.status(200).send(post);
       
    })

    //get all posts created by User
    .get('/user/allposts',
     authMiddleware,
      async (req, res) => {
        const userId = req.user.id;
        const {error, post} = await postsService.readAllUserPosts(postData)(userId);
        if (error === serviceErrors.RECORD_NOT_FOUND) {
            return res.status(404).send({ message: 'Post not found!' });
        } else {
            res.status(200).send(post);
        }
    })

    //Is this post liked by the logged user?
    .get('/likes/user',
    authMiddleware,
    async (req, res) => {
        const userId = req.user.id;
        const post = await postsService.AllPostsLikedByTheUser(postData)(userId); 
        res.status(200).send(post);  
    });

    export default postsController;