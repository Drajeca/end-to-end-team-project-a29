import express from 'express';
import commentData from './../data/comments-data.js';
import { createValidator, createCommentSchema } from '../validations/index.js';
import serviceErrors from '../services/service-errors.js';
import commentsService from '../services/comments-service.js';
import { authMiddleware, roleMiddleware } from '../auth/auth-middleware.js';
import { ADMIN_USER_ROLE, DEFAULT_USER_ROLE } from '../config.js';
import commentsData from './../data/comments-data.js';

const commentsController = express.Router();

commentsController
    // create comment
    .post('/:id/comments',    
    authMiddleware,
    roleMiddleware([DEFAULT_USER_ROLE, ADMIN_USER_ROLE]),
    createValidator(createCommentSchema),
    async (req, res) => {
        const createData = req.body;
        createData.userId = req.user.id;
        const { id } = req.params;
        createData.postId = id;

        const post = await commentsService.createNewComment(commentData)(createData);
        res.status(201).send(post);
        
    })

    // delete comment
    .delete('/:postId/comments/:commentId',
    authMiddleware,
    roleMiddleware([DEFAULT_USER_ROLE, ADMIN_USER_ROLE]),
    async (req, res) => {

        const { postId, commentId } = req.params;
        const userId = req.user.id;
        const role = req.user.role;
        const { error, comment } = await commentsService.deleteComment(commentData)(+postId, commentId, userId, role);

        if(error === serviceErrors.OPERATION_NOT_PERMITTED) {
            res.status(400).send({ message: 'You are not allowed to delete other\'s people comments!' });
        }
         else if (error === serviceErrors.RECORD_NOT_FOUND) {
            res.status(404).send({ message: 'Comment not found!' });
        }
         else {
            res.status(201).send(comment);
        }
    })

    // update comment
    .put('/:postId/comments/:commentId',
    authMiddleware,
    roleMiddleware([DEFAULT_USER_ROLE, ADMIN_USER_ROLE]),
    createValidator(createCommentSchema),
    async (req, res) => {

        const { postId, commentId } = req.params;
        const userId = req.user.id;
        const role = req.user.role;
        const content = req.body.content;
        const { error, comment } = await commentsService.updateComment(commentData)(+postId, commentId, userId, role, content);

        if(error === serviceErrors.OPERATION_NOT_PERMITTED) {
            res.status(400).send({ message: 'You are not allowed to update other\'s people comments!' });
        }
         else if (error === serviceErrors.RECORD_NOT_FOUND) {
            res.status(404).send({ message: 'Comment not found!' });
        }
         else {
            res.status(201).send(comment);
        }
    })

    //get all post comments
    .get('/:postId/comments', async (req, res) => {
        const { postId } = req.params;
        const { error, comment } = await commentsService.readComments(commentData)(+postId);
        if (error === serviceErrors.RECORD_NOT_FOUND) {
            res.status(404).send({ message: 'Comment not found!' });
       } else {
            res.status(200).send(comment);
       }
    })

    //get individual comment
    .get('/:postId/comments/:commentId', async (req, res) => {
        const { postId, commentId } = req.params;
        const { error, comment } = await commentsService.readIndividualComment(commentData)(+postId, +commentId);
        if (error === serviceErrors.RECORD_NOT_FOUND) {
            res.status(404).send({ message: 'Comment not found!' });
       } else {
            res.status(200).send(comment);
       }
    })    

     // like comment
     .post('/:postId/comments/:commentId/likes',
     authMiddleware,
     roleMiddleware([DEFAULT_USER_ROLE, ADMIN_USER_ROLE]),
     async (req, res) => {
        const { postId, commentId } = req.params;
        const userId = req.user.id;
 
         const { error, comment } = await commentsService.likeComment(commentsData)(+postId, +commentId, userId);
         
         if(error === serviceErrors.RECORD_NOT_FOUND) {
             return res.status(400).send({ message: 'You are trying to like a comment that doesn\'t exist!' });
         }
 
         if(error === serviceErrors.DUPLICATE_RECORD) {
             return res.status(409).send({ message: 'You already liked that comment!'});
         }
             res.status(200).send(comment);
     })

     .get('/comments/likes/user',
     authMiddleware,
     async (req, res) => {
         const userId = req.user.id;
         const post = await commentsService.AllCommentsLikedByTheUser(commentData)(userId); 
         res.status(200).send(post);  
     });


    export default commentsController;