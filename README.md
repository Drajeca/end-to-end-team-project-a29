# End-to-end Team Project a29

## Project Name & Pitch

Post(a)holic9

A forum application, built with React, Express, JavaScript, and CSS.

## Project Status

This project is currently in development.
1. Users: 
- Users can create/edit/delete posts and filter them by title. 
- Users can create/edit/delete post comments.
- Users can also like post/comments.
2. Admins:
- Have all  user privileges.
- Admins can lock posts
- Admins can ban users.
- Admins can delete users.
- Admins can filter users by their username.

3. Every user/admin has their own My Posts tab, where they can track their posts.
4. Login/Logout functionality.
5. Register new user functionality.

## Project Screen Shot(s)

#### Example:

<img src='client/postaholic9/public/screenshots/Screenshot-login.png'>
<br /><br />
<img src='client/postaholic9/public/screenshots/Screenshot-posts.png'>
<br /><br />
<img src='client/postaholic9/public/screenshots/Screenshot-admin.png'>
<br /><br />
<img src='client/postaholic9/public/screenshots/Screenshot-register.png'>
<br /><br />

## Installation and Setup Instructions

1. Database
  - Import database from database folder into My SQL workbench.
  - Don't forget to change your credentials in the config.js file, located in the client folder.

2. Server-side
  - Navigate to the root level aka project_main and install node modules.
  - You can start the server by executing the following command: npm start / npm run start:dev
  - Server servers on port 5001.

3. Client-side

  - Navigate to the root level aka client/postaholic9 and install node modules.
  - You can start the client by executing the following command: npm start
  - You can access the application upon entering localhost:3000.

4. Admin login
  - If you want to try an admin profile you can use: **username: admin, password: admin1**
