CREATE DATABASE  IF NOT EXISTS `projectdb` /*!40100 DEFAULT CHARACTER SET utf8mb3 */;
USE `projectdb`;
-- MariaDB dump 10.19  Distrib 10.6.2-MariaDB, for Win64 (AMD64)
--
-- Host: 127.0.0.1    Database: projectdb
-- ------------------------------------------------------
-- Server version	10.6.2-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `comment_likes`
--

DROP TABLE IF EXISTS `comment_likes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comment_likes` (
  `commentId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  PRIMARY KEY (`commentId`,`userId`),
  KEY `fk_commentlikes_users1_idx` (`userId`),
  CONSTRAINT `fk_commentlikes_comments` FOREIGN KEY (`commentId`) REFERENCES `comments` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_commentlikes_users1` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comment_likes`
--

LOCK TABLES `comment_likes` WRITE;
/*!40000 ALTER TABLE `comment_likes` DISABLE KEYS */;
INSERT INTO `comment_likes` VALUES (65,10),(65,14),(66,10),(66,14),(67,10),(67,14),(68,10),(69,10),(69,14),(70,10),(72,14),(74,14),(75,14),(77,14),(78,14),(79,14),(81,10),(83,14),(83,15),(84,10),(84,14),(87,10),(87,14),(88,14),(89,14),(90,10),(91,10),(92,10),(92,14),(93,10),(94,14),(95,14),(96,14),(98,14),(99,14),(100,14),(101,14),(102,14),(103,14),(104,14),(105,14),(106,14),(107,14),(108,14),(109,14),(110,14),(111,14),(112,14),(113,14),(114,14),(115,14),(116,14),(117,14),(118,14),(119,14),(120,14),(121,14),(122,14),(123,14),(124,14),(125,14),(126,10),(126,14),(127,10),(127,14),(128,10),(128,14),(129,10),(129,14),(130,14),(131,10),(131,14),(132,14),(133,10),(134,10),(135,10),(135,14),(136,10),(137,10),(140,10),(140,38),(141,10),(146,14),(147,14),(148,14),(149,14),(150,14),(151,14),(153,14),(153,20),(154,14),(156,22),(157,22),(157,23),(159,20),(160,10),(160,14),(161,14),(162,14),(163,14),(164,14),(165,14),(166,10),(166,14),(167,10),(167,14),(167,23),(168,10),(169,14),(170,14),(172,23),(178,14),(179,14),(181,14),(183,14),(184,14);
/*!40000 ALTER TABLE `comment_likes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments` (
  `content` varchar(255) NOT NULL,
  `isDeleted` tinyint(1) NOT NULL DEFAULT 0,
  `createdOn` datetime NOT NULL DEFAULT current_timestamp(),
  `userId` int(11) NOT NULL,
  `postId` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `fk_comments_users1_idx` (`userId`),
  KEY `fk_comments_posts1_idx` (`postId`),
  CONSTRAINT `fk_comments_posts1` FOREIGN KEY (`postId`) REFERENCES `posts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_comments_users1` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=186 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments`
--

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
INSERT INTO `comments` VALUES ('updated content',0,'2021-07-01 11:19:43',5,1,1),('1',0,'2021-07-04 14:39:56',10,1,2),('Vrubdz',1,'2021-07-04 14:43:57',10,1,3),('Vrubz content',1,'2021-07-04 14:55:03',10,1,4),('admin comment',0,'2021-07-10 15:07:04',14,1,5),('admin comment2',0,'2021-07-10 15:13:40',14,1,6),('nov komentar',0,'2021-07-18 14:57:21',14,1,7),('nov komentar',0,'2021-07-18 14:57:38',14,6,8),('nov komentar',1,'2021-07-18 14:58:05',14,2,9),('oshte edin komentar',1,'2021-07-18 14:59:25',14,2,10),('sdfsadf',0,'2021-07-18 15:01:35',14,6,11),('ahhaahah',0,'2021-07-18 17:03:37',14,1,12),('pyrvi komentar',0,'2021-07-18 17:05:28',14,26,13),('nov komentar',0,'2021-07-18 18:38:50',14,1,14),('asdfasdfasdf',0,'2021-07-18 18:50:59',14,2,15),('asdfasdfasdfsadf',1,'2021-07-18 18:51:02',14,2,16),('asdfasfadfadfasdfa',1,'2021-07-18 18:51:05',14,2,17),('sadfasdfadfasd',0,'2021-07-18 18:51:18',14,2,18),('asdfasdfadfasfdad',0,'2021-07-18 18:51:21',14,2,19),('adfadfadfasdfasdfdasd',1,'2021-07-18 18:51:24',14,2,20),('nov komentar',0,'2021-07-18 18:53:40',14,2,21),('naj noviq komentar',1,'2021-07-18 18:54:49',14,2,22),('nov komentar',1,'2021-07-18 19:08:23',14,2,23),('first!!!!',1,'2021-07-18 19:11:41',14,8,24),('second, first comment!',0,'2021-07-18 19:13:02',14,8,25),('някъв коментар!',1,'2021-07-18 19:26:18',14,2,26),('nov komenasdgsadga',0,'2021-07-18 20:28:02',14,2,27),('wazaaa',0,'2021-07-18 21:35:01',14,3,28),('sdf',1,'2021-07-18 21:35:15',14,3,29),('nov komentar',1,'2021-07-18 21:37:11',14,3,30),('hello',0,'2021-07-18 21:38:35',14,4,31),('fwewe',0,'2021-07-18 21:43:38',14,3,32),('sadfasdafsadf',1,'2021-07-19 08:13:42',14,3,33),('qffq',1,'2021-07-19 09:00:04',14,3,34),('asasas',1,'2021-07-19 09:05:00',14,3,35),('updated comment',0,'2021-07-19 11:22:50',14,5,36),('nqkakwi neshta',0,'2021-07-19 11:23:20',14,5,37),('asdasddasdsa',1,'2021-07-19 11:50:37',14,5,38),('aaaaaaaaaaaaa',0,'2021-07-19 12:11:57',14,5,39),('pyrvi komentar!!!!',1,'2021-07-19 13:25:34',14,37,40),('nov komentar',0,'2021-07-19 13:25:50',14,37,41),('asasasas',0,'2021-07-19 13:49:10',14,5,42),('nov komentar',0,'2021-07-19 13:50:26',14,6,43),('dghfasdjfweytrw',1,'2021-07-19 13:52:13',14,6,44),('asdasdaaaaa',0,'2021-07-19 13:53:33',14,16,45),('asdasdazaza',0,'2021-07-19 13:53:41',14,31,46),('pyrvi komentar',0,'2021-07-19 14:19:14',14,9,47),('novnov asd',0,'2021-07-19 18:58:23',14,6,48),('updated comment',0,'2021-07-19 19:15:14',14,7,49),('third  comment',0,'2021-07-19 19:15:33',14,8,50),('asdaaaaa',1,'2021-07-19 19:37:51',14,7,51),('casacsa',0,'2021-07-19 19:56:34',14,7,52),('asdf',1,'2021-07-19 19:57:48',14,8,53),('asd',0,'2021-07-19 19:59:01',14,8,54),('sdcas',1,'2021-07-19 20:10:57',14,12,55),('hello',0,'2021-07-19 20:11:07',14,12,56),('asdasda',0,'2021-07-20 09:36:45',14,12,57),('sasasas',0,'2021-07-20 10:58:27',14,16,58),('qdqwd',1,'2021-07-20 11:07:47',14,31,59),('асасaaaaaa',0,'2021-07-20 13:20:25',14,31,60),('nov komentar',1,'2021-07-20 16:19:46',14,31,61),('asdasd',1,'2021-07-21 11:03:20',14,48,62),('asdasd',0,'2021-07-21 11:03:24',14,48,63),('asdasdasd',1,'2021-07-21 12:15:07',14,31,64),('asdasdxaxa',0,'2021-07-21 13:43:35',14,73,65),('aaa',0,'2021-07-21 14:25:50',14,73,66),('ssss',0,'2021-07-21 15:47:17',14,73,67),('асдасд',0,'2021-07-21 16:18:59',14,89,68),('asdasdas',0,'2021-07-22 11:01:43',14,77,69),('new comment',1,'2021-07-22 11:04:28',10,90,70),('new comment',0,'2021-07-22 11:06:20',14,90,71),('sdasd',0,'2021-07-22 13:27:28',14,74,72),('asdasd',0,'2021-07-22 13:29:27',14,74,73),('asdasdasdadsad',0,'2021-07-22 13:45:36',14,75,74),('asdfasf',0,'2021-07-23 11:02:41',14,83,75),('dasdasd',1,'2021-07-23 11:25:03',10,75,76),('asdasd',0,'2021-07-23 11:25:12',10,75,77),('asdasd',0,'2021-07-23 11:25:17',10,75,78),('asdasd',0,'2021-07-23 11:25:19',10,75,79),('asdasd',1,'2021-07-23 11:25:28',10,77,80),('asdas',0,'2021-07-23 11:25:32',10,77,81),('asdasd',1,'2021-07-23 11:26:06',10,96,82),('asdasd',0,'2021-07-23 11:26:12',10,96,83),('asdas',0,'2021-07-23 11:26:19',10,75,84),('asd',1,'2021-07-23 11:38:40',14,97,85),('asd',0,'2021-07-23 11:38:44',14,97,86),('asdasdasd',0,'2021-07-23 11:38:50',14,75,87),('asdasd',0,'2021-07-23 12:37:47',14,99,88),('asdasd',0,'2021-07-23 12:47:52',14,75,89),('nov komentar',0,'2021-07-23 12:48:23',10,98,90),('asd',0,'2021-07-23 12:48:42',10,94,91),('asd',0,'2021-07-23 12:48:55',10,75,92),('novo',1,'2021-07-23 12:50:31',10,77,93),('golqma diskusiq se vodi tuk няма как',0,'2021-07-23 12:52:12',15,75,94),('csasaca',1,'2021-07-23 15:34:16',14,102,95),('asdasads',0,'2021-07-23 22:42:13',14,102,96),('asdasds',0,'2021-07-23 22:42:18',14,102,97),('nov komentar',0,'2021-07-24 11:11:37',14,104,98),('asdasd',0,'2021-07-24 14:32:13',14,104,99),('wefweqfew',0,'2021-07-25 11:18:12',14,100,100),('errwreqewrqwer',1,'2021-07-25 20:19:33',14,108,101),('fAFSFa',1,'2021-07-25 20:19:36',14,108,102),('qwqwqw',1,'2021-07-26 14:29:30',14,107,103),('wqwqwqw',0,'2021-07-26 14:29:34',14,107,104),('eqweqweqe',1,'2021-07-26 15:42:47',14,107,105),('ssss',1,'2021-07-26 15:46:53',14,107,106),('aaaa',0,'2021-07-26 15:48:56',14,107,107),('asdasd',0,'2021-07-26 15:53:42',14,107,108),('asdas',1,'2021-07-26 15:55:22',14,107,109),('asdasd',1,'2021-07-26 15:56:04',14,96,110),('aa',0,'2021-07-26 15:56:39',14,100,111),('asdasd',1,'2021-07-26 16:06:33',14,100,112),('asdasd',0,'2021-07-26 16:06:56',14,100,113),('asdasd',0,'2021-07-26 16:11:18',14,100,114),('q',0,'2021-07-26 16:14:41',14,100,115),('aaa',1,'2021-07-26 16:19:52',14,107,116),('asdasdfdafsadf',1,'2021-07-26 16:25:37',14,109,117),('ffffffffffffffggg',0,'2021-07-26 16:26:08',14,109,118),('aaaa',0,'2021-07-26 16:35:43',14,109,119),('asdasdasd',0,'2021-07-26 16:50:42',14,109,120),('asdasdas2222',0,'2021-07-26 16:51:33',14,109,121),('pyrvi komentar',0,'2021-07-26 16:53:04',14,109,122),('sd',0,'2021-07-26 17:31:50',14,109,123),('wqeqweqwe',0,'2021-07-26 17:36:09',14,109,124),('asdasdasdasd123123123',1,'2021-07-27 16:03:05',14,107,125),('asdasdas',0,'2021-07-27 18:06:53',14,111,126),('asdasd',0,'2021-07-27 18:37:09',10,112,127),('wasf',0,'2021-07-27 18:57:33',14,112,128),('asd',1,'2021-07-27 19:20:47',14,113,129),('sefwefeewrwe',1,'2021-07-27 19:23:13',14,113,130),('asasdas',0,'2021-07-27 19:31:03',14,113,131),('nekakyw si',0,'2021-07-28 00:22:24',14,75,132),('asdasd13123',1,'2021-07-28 00:23:21',10,113,133),('asdqwe',0,'2021-07-28 00:57:40',10,86,134),('nov koment',0,'2021-07-28 10:08:24',14,113,135),('much wow ',0,'2021-07-28 10:20:15',10,111,136),('asd',0,'2021-07-28 10:26:29',10,112,137),('dasdas',0,'2021-07-28 12:49:03',10,115,138),('123asad',0,'2021-07-28 17:45:24',14,116,139),('asasxzxzx',0,'2021-07-29 01:35:41',38,117,140),('asdasdasd123123',0,'2021-07-29 10:45:34',10,113,141),('asdasdasd',0,'2021-07-29 13:58:30',14,121,142),('asdasdasd',0,'2021-07-29 13:59:01',14,121,143),('asdasdasd',0,'2021-07-29 13:59:41',14,121,144),('a',0,'2021-07-29 14:07:44',14,121,145),('comment',0,'2021-07-29 15:08:51',14,123,146),('asfhjf sdhfjskdh f djksfs\nsjdghjkasgkldaS\nAsjhdAJKSHDAds\naslkhdlkas;DS',0,'2021-07-29 15:41:25',14,119,147),('asdasdas',0,'2021-07-29 17:06:07',14,123,148),('asd',1,'2021-07-29 17:09:35',14,124,149),('adasda\nasdasd\nasdas\ndasdasd',1,'2021-07-29 17:17:04',14,124,150),('adasdasds',1,'2021-07-29 19:31:57',14,124,151),('asdasdas',1,'2021-07-29 19:34:48',14,125,152),('asdasdas',0,'2021-07-29 20:43:51',14,120,153),('asdasdasd',0,'2021-07-29 21:19:49',14,125,154),('sfDF',0,'2021-07-30 02:19:34',22,127,155),('asdSa asd sdasd asd',0,'2021-07-30 02:19:38',22,125,156),('komentari',0,'2021-07-30 02:24:49',22,78,157),('asdasd',0,'2021-07-30 11:22:55',14,128,158),('asd',0,'2021-07-30 11:35:52',20,129,159),('asffsd',0,'2021-07-30 14:53:37',14,149,160),('fsadfsd',0,'2021-07-30 15:10:48',14,146,161),('fwefqwefqwe',1,'2021-07-30 18:45:28',14,154,162),('asdasdas',1,'2021-07-30 19:11:29',14,154,163),('fwfeqqw',0,'2021-07-30 19:11:47',14,152,164),('wefqewfqwe',0,'2021-07-30 19:21:33',14,154,165),('wefweew',0,'2021-07-30 19:42:47',14,154,166),('wefwef213123',0,'2021-07-30 20:28:44',14,155,167),('asdSAD123123',1,'2021-07-30 20:29:41',10,147,168),('weqsdASD',1,'2021-07-30 21:09:05',14,155,169),('wrerw',1,'2021-07-31 09:41:51',14,156,170),('асдасд',1,'2021-07-31 11:15:02',14,156,171),('asdasd',0,'2021-07-31 12:18:32',14,155,172),('asdasd',1,'2021-07-31 12:23:25',14,157,173),('asdasdas',1,'2021-07-31 14:12:50',14,157,174),('dsdvadsvasd',1,'2021-07-31 14:13:28',14,157,175),('asdasd',0,'2021-07-31 14:30:23',14,157,176),('asadsd12312asdda',0,'2021-07-31 14:45:08',14,158,177),('asca213123123DSF234S',0,'2021-07-31 14:58:30',14,151,178),('asd123sd23',1,'2021-07-31 15:31:39',14,155,179),('as',0,'2021-07-31 17:37:37',14,159,180),('asd',1,'2021-07-31 18:14:11',14,155,181),('d',0,'2021-07-31 18:16:25',14,159,182),('asdasdasd',1,'2021-07-31 21:51:35',14,161,183),('dsadSsd',1,'2021-07-31 21:51:38',14,161,184),('              ',1,'2021-07-31 22:45:29',21,161,185);
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post_likes`
--

DROP TABLE IF EXISTS `post_likes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `post_likes` (
  `postId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  PRIMARY KEY (`postId`,`userId`),
  KEY `fk_postLikes_posts_idx` (`postId`),
  KEY `fk_postLikes_users1_idx` (`userId`),
  CONSTRAINT `fk_postLikes_posts1` FOREIGN KEY (`postId`) REFERENCES `posts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_postLikes_users1` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post_likes`
--

LOCK TABLES `post_likes` WRITE;
/*!40000 ALTER TABLE `post_likes` DISABLE KEYS */;
INSERT INTO `post_likes` VALUES (73,10),(73,14),(73,17),(73,18),(74,10),(74,14),(74,17),(74,18),(75,14),(75,17),(75,18),(75,23),(76,14),(76,17),(76,23),(77,14),(77,17),(77,23),(78,14),(78,18),(78,22),(79,10),(79,14),(80,14),(80,18),(80,22),(81,14),(81,18),(81,23),(82,10),(82,14),(82,18),(82,23),(83,14),(83,17),(83,23),(84,14),(84,17),(84,18),(85,14),(85,18),(85,23),(86,14),(86,18),(86,23),(87,14),(87,18),(87,19),(88,14),(88,17),(88,18),(89,14),(89,23),(90,10),(90,14),(90,23),(94,10),(94,14),(95,14),(96,10),(96,15),(96,23),(97,14),(98,10),(98,14),(98,23),(99,14),(100,14),(100,15),(101,14),(101,19),(102,14),(102,19),(103,14),(104,14),(105,14),(105,23),(106,14),(106,22),(106,23),(107,14),(107,23),(108,14),(109,14),(110,14),(110,20),(110,22),(110,23),(111,10),(111,14),(111,20),(111,22),(111,23),(112,10),(112,14),(112,20),(112,23),(113,10),(113,14),(113,20),(113,23),(114,14),(116,14),(117,14),(117,38),(119,14),(119,22),(119,23),(120,14),(120,20),(120,23),(121,14),(121,20),(121,23),(123,14),(123,20),(123,22),(124,14),(125,14),(125,20),(125,22),(126,20),(127,14),(127,22),(128,14),(129,20),(129,23),(130,20),(130,23),(131,23),(132,23),(133,23),(134,14),(134,23),(135,14),(135,23),(136,14),(136,23),(137,14),(137,23),(138,14),(138,23),(139,23),(140,14),(140,21),(140,23),(141,10),(141,14),(141,23),(142,14),(142,23),(143,10),(143,14),(143,23),(144,14),(144,23),(145,10),(145,14),(145,23),(146,10),(146,14),(146,23),(147,10),(147,14),(147,23),(148,10),(148,14),(148,23),(149,10),(149,14),(149,23),(150,10),(150,14),(151,10),(151,14),(152,10),(152,14),(152,21),(152,23),(153,14),(154,10),(154,14),(154,20),(155,10),(155,14),(155,20),(155,23),(156,14),(156,20),(157,14),(159,14),(160,14),(161,14),(162,14);
/*!40000 ALTER TABLE `post_likes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) NOT NULL,
  `content` varchar(255) NOT NULL,
  `userId` int(11) NOT NULL,
  `createdOn` datetime NOT NULL DEFAULT current_timestamp(),
  `isDeleted` tinyint(1) NOT NULL DEFAULT 0,
  `isLocked` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `fk_posts_users_idx` (`userId`),
  CONSTRAINT `fk_posts_users` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=163 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` VALUES (1,'admin change','updated content',5,'2021-07-01 11:17:00',1,0),(2,'asdqwe123','asdqwe123',10,'2021-07-03 12:39:18',1,0),(3,'New original title changed','New Original ',10,'2021-07-03 14:11:54',1,0),(4,'New original title2','New Original Content',10,'2021-07-03 14:40:38',1,0),(5,'12345123123','1234512123',10,'2021-07-03 14:49:52',1,0),(6,'123456asd','12345a',10,'2021-07-03 14:51:33',1,0),(7,'editpost','editpost',10,'2021-07-03 14:56:53',1,0),(8,'test test1231','You should be created by the logged user!123',10,'2021-07-03 15:00:03',1,0),(9,'Vrubdz','Vrubdz',10,'2021-07-04 14:23:38',1,0),(10,'some new title','some new content',10,'2021-07-10 11:27:54',1,0),(11,'admin title','admin content',14,'2021-07-10 15:00:16',1,0),(12,'banned title','banned content',10,'2021-07-10 18:15:20',1,0),(13,'yet another post','yet another content',14,'2021-07-16 18:02:39',1,0),(14,'testing some stugg','asdfasdfasdf',14,'2021-07-16 20:24:20',1,0),(15,'Eureka!!!!','somehow this sh.t works!??!?',14,'2021-07-16 20:25:23',1,0),(16,'asdaaa','asdaaa',14,'2021-07-16 20:36:31',1,0),(17,'qqq','asd',14,'2021-07-16 21:26:12',1,0),(18,'asda','asdas',14,'2021-07-16 21:26:54',1,0),(19,'asdaa','asdas',14,'2021-07-16 21:26:56',1,0),(20,'new post','ahaahah',14,'2021-07-16 21:39:33',1,0),(24,'SADJKGHSDHAGL','SAKJLDHGKASJLHDGKAS',14,'2021-07-18 10:20:12',1,0),(25,'DSADFASD','ASDFASDF',14,'2021-07-18 10:20:51',1,0),(26,'nov post','nov content',14,'2021-07-18 17:05:13',1,0),(27,'post to delete`','post to delete',14,'2021-07-18 18:13:49',1,0),(28,'ASDasdSD','asdfasdfadfasd',14,'2021-07-18 18:51:54',1,0),(29,'dfdfwq','fqwdfqwef',14,'2021-07-18 19:08:05',1,0),(30,'postche','asdqwe123',14,'2021-07-18 19:26:33',1,0),(31,'nov postswcwc','nov komentarwcwec',14,'2021-07-18 21:38:47',1,0),(32,'bnmghjyui','bnmhjkiuoo',14,'2021-07-18 21:55:09',1,0),(33,'aaaaaaaaaaa','aaaaaaaaa',14,'2021-07-18 21:59:23',1,0),(34,'aaaaaaaaaaaaaaaaaa','aaaaaaaaaaaaaaaaa',14,'2021-07-18 22:00:50',1,0),(35,'a123','a123',14,'2021-07-18 22:02:11',1,0),(36,'new post','asasas',14,'2021-07-19 09:05:06',1,0),(37,'naj noviq post 6000','naj noviq komentar',14,'2021-07-19 13:25:10',1,0),(38,'novo postche','nekakvi super vajnite raboti',14,'2021-07-19 14:17:09',1,0),(39,'qwerty','qwerty',14,'2021-07-19 19:20:37',1,0),(40,'asdfasdfasdfasdf','asdfasdfadfasdf',14,'2021-07-19 19:28:42',1,0),(41,'new post','new comment',14,'2021-07-20 09:36:29',1,0),(42,'асдасд','аасдасдас',14,'2021-07-20 10:58:00',1,0),(43,'wefqwef','wfqwefq',14,'2021-07-20 11:14:08',1,0),(44,'asdasd','asdasd',14,'2021-07-20 12:08:38',1,0),(45,'nov nov','post post',14,'2021-07-20 16:12:29',1,0),(46,'asd','asd',14,'2021-07-20 16:19:29',1,0),(47,'new entry123','new entry22323',14,'2021-07-20 19:43:11',1,0),(48,'posttolike','asdasdasd',14,'2021-07-20 20:18:36',1,0),(49,'aaaaaaaaa','aaaaaaa',14,'2021-07-20 20:22:24',1,0),(50,'bbbbbbbbbb','bbbbbbb',14,'2021-07-20 20:23:42',1,0),(51,'asdasdasdasd','asdasdasdasda',14,'2021-07-20 20:30:00',1,0),(52,'asdqweasd','asdqweasd',14,'2021-07-20 20:43:38',1,0),(53,'4321','4321',14,'2021-07-20 20:44:45',1,0),(54,'asdasdasd','asdasdasdas',14,'2021-07-20 21:22:41',1,0),(55,'ada13213','asdasd123123',14,'2021-07-20 21:25:20',1,0),(56,'aaaaaa','aaaaa',14,'2021-07-20 21:31:52',1,0),(57,'aaaaa','aaaaaa',14,'2021-07-20 21:37:01',1,0),(58,'ttttt','tttttt',14,'2021-07-21 08:31:54',1,0),(59,'1231234444','123123123',14,'2021-07-21 08:40:56',1,0),(61,'eeeeeeeeee','eeeeeeeeee',14,'2021-07-21 08:46:34',1,0),(63,'явеявеяве','явеявеяве',14,'2021-07-21 09:05:41',1,0),(64,'qqq','qqq',14,'2021-07-21 09:12:30',1,0),(65,'1111','11111',14,'2021-07-21 09:23:25',1,0),(66,'fffffffff','fffffffffff',14,'2021-07-21 09:28:50',1,0),(67,'vvvvv','vvvvvv',14,'2021-07-21 09:30:35',1,0),(68,'sadasda','asdasdas',14,'2021-07-21 09:32:46',1,0),(69,'asdsda','asdasdasd',14,'2021-07-21 09:38:16',1,0),(70,'rwer','wer',14,'2021-07-21 09:40:58',1,0),(71,'werwerw','werwerwer',14,'2021-07-21 09:41:20',1,0),(72,'dasdasd','asdasdasd',14,'2021-07-21 09:42:58',1,0),(73,'432432----','543543',14,'2021-07-21 12:15:20',1,0),(74,'asa','asas',14,'2021-07-21 12:55:34',1,0),(75,'aaa','aaaa',14,'2021-07-21 13:42:14',0,0),(76,'235782538','2653752872',14,'2021-07-21 13:42:35',0,0),(77,'aaaaaaa','aaaaaaa',14,'2021-07-21 13:43:25',0,0),(78,'asdasd','asdasdasd',14,'2021-07-21 13:43:43',0,0),(79,'asda','asdas',14,'2021-07-21 13:45:06',0,0),(80,'asdas','asdasd',14,'2021-07-21 13:45:20',0,0),(81,';;;;;;;',';;;;;;;;;;',14,'2021-07-21 13:46:32',0,0),(82,'...........','............',14,'2021-07-21 13:50:05',0,0),(83,'0987654','987654',14,'2021-07-21 14:24:46',0,0),(84,'hgjghj','ghjghj',14,'2021-07-21 14:29:26',0,0),(85,'nov post 6000','hahah',14,'2021-07-21 15:43:54',0,0),(86,'yukyukmy','myyumkyk',14,'2021-07-21 15:44:31',0,0),(87,'66666','66666',14,'2021-07-21 15:47:59',0,0),(88,'lllllll','lllllllll',14,'2021-07-21 15:51:07',0,0),(89,'876547654','87654765',14,'2021-07-21 16:18:41',0,0),(90,',.,.,.',',.,,.',14,'2021-07-21 16:24:31',0,0),(93,'------------','-----------',14,'2021-07-21 16:43:06',1,0),(94,'novnov','novnov',10,'2021-07-22 11:12:38',1,0),(95,'asd','asd',14,'2021-07-22 12:29:10',1,0),(96,'123123','123123',10,'2021-07-23 11:26:00',0,0),(97,'aaaaaaaaaaaa','aaaaaaaaaaaa',14,'2021-07-23 11:38:35',0,0),(98,'Novpost6000','askdjafa',14,'2021-07-23 11:50:40',0,0),(99,'novnovnov','novnovnov',14,'2021-07-23 12:37:38',1,0),(100,'nov post','nov post',15,'2021-07-23 12:51:44',0,0),(101,'naj nov','post',15,'2021-07-23 12:59:22',1,0),(102,'naj nov2','naj',15,'2021-07-23 13:00:00',1,0),(103,'aaaaa','aaaa',14,'2021-07-23 15:48:38',1,0),(104,'skdahs kasdhAKSGHD ASJKDHKAs','djfkjsfdhflasdhfsldk sdhfksadhfkashdl shdfasdhfkasdhfaklsdhfalksdhflkadhfaldkhfalsdfhaslkdf',14,'2021-07-24 10:54:54',1,0),(105,'asdasdasd123','asdasdasd123',14,'2021-07-25 14:40:07',0,0),(106,'asd','asd',14,'2021-07-25 14:58:07',0,0),(107,'asdasdas----','asdasd------',14,'2021-07-25 15:01:39',0,0),(108,'asdasda','asdasdasd',14,'2021-07-25 15:03:17',1,0),(109,'asdqweasd','qweasdqwe',14,'2021-07-26 16:23:46',1,0),(110,'aaaa123123','123123123',14,'2021-07-27 16:51:29',0,0),(111,'9090909','090909',14,'2021-07-27 17:43:29',0,0),(112,'nov post za lock','lock',10,'2021-07-27 18:33:24',1,0),(113,'tytytysdas','tytytydds',10,'2021-07-27 18:49:47',0,1),(114,'aaaaaaaaa---','aaaaaaaaaaaaaaaaaa----',14,'2021-07-28 00:13:58',1,1),(115,'asdasda','asdasdasd',10,'2021-07-28 10:55:14',1,0),(116,'gfj345','gjt55324',23,'2021-07-28 15:44:51',1,0),(117,'hello','bye',38,'2021-07-29 01:35:35',0,1),(118,'a','a',14,'2021-07-29 11:17:08',1,0),(119,'a','a',14,'2021-07-29 11:18:47',0,0),(120,'ab','a',14,'2021-07-29 11:20:51',0,0),(121,'890890','890890',14,'2021-07-29 11:36:02',0,0),(122,' ',' ',14,'2021-07-29 14:20:04',1,0),(123,'postpost','postpost',14,'2021-07-29 15:08:45',0,0),(124,'asdasd123asdS','asdasdasdasdD',14,'2021-07-29 17:09:27',1,0),(125,'asdasdasd','sdfsdfasdfasdsadasd',14,'2021-07-29 19:34:36',0,0),(126,'nov post shtoto si pich','i taka primerno',20,'2021-07-29 22:18:24',1,0),(127,'asgdha asdgasggdsa jasgdjgsd','asdfsdfsdf\ndfadfasd\nfa\nsd\nfad\nfad',22,'2021-07-30 02:19:23',0,1),(128,'asdasdas','asdasdasdas',14,'2021-07-30 11:22:50',1,0),(129,'asd1111','asd',20,'2021-07-30 11:35:24',0,0),(130,'789789567534','sdfhjukyu',20,'2021-07-30 11:35:59',0,0),(131,'asdasd12312312','asdasdasd',23,'2021-07-30 12:38:20',0,0),(132,'12312123','123123123123',23,'2021-07-30 12:41:29',0,0),(133,'asdasd123123','123123123123',23,'2021-07-30 12:43:21',0,0),(134,'hjlhl','lghjlgjl',23,'2021-07-30 12:49:10',0,0),(135,'asasd12312','sdasd231231',23,'2021-07-30 12:54:04',0,0),(136,'sassasas','assasa',23,'2021-07-30 13:04:50',0,0),(137,'78967856','5678578965',23,'2021-07-30 13:20:05',0,0),(138,'asdasd312312','asdasda',23,'2021-07-30 13:37:42',0,0),(139,'5626256','456345656',23,'2021-07-30 14:08:04',0,0),(140,'09870987','098098',23,'2021-07-30 14:09:51',0,0),(141,'4357345','35735473',23,'2021-07-30 14:13:21',0,0),(142,'ererwer','qwerqwerqw',23,'2021-07-30 14:15:05',0,0),(143,'84784','47868',23,'2021-07-30 14:19:29',0,0),(144,'afgafga','afgafaga',23,'2021-07-30 14:31:14',0,0),(145,'78746858568','43537457753475',23,'2021-07-30 14:32:34',0,0),(146,'562562356','eewegegege',23,'2021-07-30 14:34:26',0,0),(147,'asdasdasd1234123412','2343214213',23,'2021-07-30 14:49:21',0,1),(148,'asdas123','asdaD123',23,'2021-07-30 14:50:12',1,0),(149,'34534656576','34523452345',23,'2021-07-30 14:50:24',1,0),(150,'gewgrew','rgwergwerg',10,'2021-07-30 17:01:10',0,1),(151,'asdasd1231231','asdaSDASD',14,'2021-07-30 18:32:19',0,0),(152,'ASDA13123','ASDA123123',14,'2021-07-30 18:32:43',0,0),(153,'asdasd1312313345','asdasd',14,'2021-07-30 18:33:42',1,0),(154,'asdasd1231231223421','asdasdasd',14,'2021-07-30 18:44:46',0,0),(155,'134trhtbwdv2345-----','q34ty42b3hbfwvds345----',14,'2021-07-30 20:28:31',0,0),(156,'wqeqw','qweqwe',14,'2021-07-30 23:49:25',1,0),(157,'new post','new post',14,'2021-07-31 12:22:57',1,0),(158,'asdas1231','asdasd12313',14,'2021-07-31 14:43:02',1,0),(159,'smislen post','taka',14,'2021-07-31 15:32:37',1,0),(160,'98765432345678','87654345678',14,'2021-07-31 18:14:20',1,0),(161,'ddshdgfasdgkjf','ddadhfjksdafhalsdfas',14,'2021-07-31 18:16:34',0,1),(162,'rwerwq44','asff234234',14,'2021-07-31 22:00:46',1,0);
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `roleId` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`roleId`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (7,'User'),(8,'Admin'),(9,'Guest');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tokens`
--

DROP TABLE IF EXISTS `tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tokens` (
  `id` int(11) NOT NULL,
  `token` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tokens`
--

LOCK TABLES `tokens` WRITE;
/*!40000 ALTER TABLE `tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `password` varchar(256) NOT NULL,
  `nickname` varchar(45) NOT NULL,
  `createdOn` datetime NOT NULL DEFAULT current_timestamp(),
  `isDeleted` tinyint(1) NOT NULL DEFAULT 0,
  `roleId` int(11) NOT NULL,
  `isBanned` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `display_name_UNIQUE` (`nickname`),
  KEY `roleId_idx` (`roleId`),
  CONSTRAINT `roleId` FOREIGN KEY (`roleId`) REFERENCES `roles` (`roleId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (5,'Pesho','123','Pesho1','2021-07-01 11:15:01',1,7,'0000-00-00 00:00:00'),(6,'Gosho','123awe','xXGoshoXx','2021-07-01 12:05:10',0,8,'2021-07-31 22:42:29'),(8,'Tosho','123asdqwe','Tosho666','2021-07-02 12:23:17',0,7,'2021-07-26 14:02:46'),(9,'Ibrahim','$2b$10$AxNYrpJECFk3QU5Y6RinLOdxwphetBRrAvaSIbYc02mkXoIPEqFca','xXIbraHXx','2021-07-02 14:16:51',0,7,'2021-07-29 20:23:10'),(10,'Totev','$2b$10$6ffxvQj24MHg/P7GArRhFuJbhFwf0uFVNR18HHkt7hK2WnKEkf3ey','hyshys','2021-07-03 12:00:02',0,7,'2021-07-31 21:53:10'),(14,'Drago','$2b$10$77oGzHuF4zXKTSZVaA7c0.WirAy1eqWFOt/lu8gpIIFpcnTt8sV2y','Drajeca','2021-07-10 11:55:17',0,8,'0000-00-00 00:00:00'),(15,'User','$2b$10$yhhKRk4V9m4h8SRITGeXDOpeuLa4nGziGbVWA4VQFaNP2juuvQKwm','xXUserxx','2021-07-20 18:51:02',0,7,'2021-07-26 16:58:49'),(16,'aaaa123','$2b$10$61zaD5OvGAgbLQrpx1Fmu.3XN8l2ix1p/FmKr54L13qm6skl83e16','aaaa123','2021-07-20 19:03:57',0,7,NULL),(17,'Pesho123','$2b$10$ugkMlrRvkvmHZvNdr92As.BTQtUCc3RjYpaivgVdFVEV11rgBTMPK','Pesho123','2021-07-21 16:46:42',0,7,'2021-07-28 10:23:15'),(18,'qwe123','$2b$10$LYN6bkG.SmBniC1cMg6iUeR5JSIK6bDNZ.Jnn3qZ.gvxGVi/9O4bS','qwe123','2021-07-21 18:52:13',0,7,NULL),(19,'Dancho','$2b$10$lcnGIdb9D7Rk9b6sZ7vpAuC.Wg6KIN15T4RRuZ.CCtLmu90sJTqW2','123','2021-07-23 22:44:54',1,7,NULL),(20,'User2','$2b$10$vqf8u4vOSn4tdp9NhXc78.cPt4oA2u5T.w2ggsVaoqHCGnvoOihLS','User2','2021-07-28 13:51:21',0,7,'2021-08-07 03:00:00'),(21,'User3','$2b$10$RUk/Ml05Y10KyY7tsyOXSOOIq1K1EtOMlyQVgY71LAmAWhB.bBF7S','User3','2021-07-28 13:52:45',0,7,NULL),(22,'User4','$2b$10$vr1mdEljE6LkeuYnRDjRDOevWpXDyJztk/soKYeBjxEaXpC2q0OES','User4','2021-07-28 13:59:00',0,7,NULL),(23,'user5','$2b$10$xwvt4sQis4LM27KJ/sC8CuO8UxaBGwNYEA.3TyvjRS313UiMQ8/N6','user5','2021-07-28 15:43:51',1,7,NULL),(30,'User22','$2b$10$vAMSGLyrFV/w37Vu8vi3AeERljUVfK6vaK1IB/QJzH1eacrXIBUK2','User22','2021-07-28 16:13:02',1,7,NULL),(34,'User222','$2b$10$7OMEcUuQNaRczQI9GwQt6O5i1s6uDxZdN56UxQbKJw20MWQutWRtO','User222','2021-07-28 16:37:29',1,7,NULL),(35,'User2423141234123','$2b$10$vCK/2Gji.Kii5QOQ.1opreAvKa.8kTDJ.FH6RNwVKsMbEvANzggkS','User22341234123','2021-07-28 16:48:54',1,7,NULL),(36,'User10','$2b$10$fihIDWNB5Gep7ifC2pJzuOLmTKLpa2p6CZ7U7uCG8Rx.tkPS.FVoG','User10','2021-07-28 17:34:12',1,7,NULL),(37,'User11','$2b$10$/RI9FUyjJ1qq3o8feuhaJecDg7.bkiqWfXwxjd5FW/I2fI/W3tlRe','user11','2021-07-29 01:29:14',1,7,NULL),(38,'user66','$2b$10$K9R2b0e6wI1NuuRqNnJxVuJ2wTJXWwEOt0NWJvDoSJ6dlLHmyYltC','user66','2021-07-29 01:35:04',1,7,NULL),(39,'admin','$2b$10$08Qcz.26GVi9gJs64ANLMeaneE/Hr1zDrHcxJ1phvXWXgtF9Ps/nu','admin','2021-08-01 19:07:19',0,8,NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-08-01 19:23:19
