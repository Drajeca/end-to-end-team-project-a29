import { useEffect, useState } from "react";
import { FormControl, InputGroup } from "react-bootstrap";
import { banUser, deleteUser, getAllUsers } from "../../requests/users";
import UserDetails from "../UserDetails/UserDetails";
import { Button } from "react-bootstrap";

const Admin = () => {
  const [users, setUsers] = useState([]);
  const [keyword, setKeyword] = useState("");
  const [error, setError] = useState(null);

  useEffect(() => {
    getAllUsers().then((data) => setUsers(data));
  }, []);

  const ban = (userId, banDuration) =>
    banDuration
      ? banUser(userId, banDuration).then((result) => {
          new Date(banDuration).toLocaleDateString() ===
          new Date().toLocaleDateString()
            ? alert("User unbanned")
            : alert(
                `Banned until: ${new Date(banDuration).toLocaleDateString()}`
              );
          const userToBan = users.findIndex((user) => user.id === userId);
          users[userToBan] = result;
          setUsers([...users]);
        }).catch(error => setError(error.message))
      : alert("Please select a ban date");

  const remove = (userId) =>
    deleteUser(userId).then((r) => {
      const result = users.filter((user) => user.id !== r.id);
      setUsers([...result]);
    });

    if (error) {
      return (
        <h4>
          <i>An error has occurred: </i>
          {error}
        </h4>
      );
    }
    
  const allUsers = users
    .filter(
      (user) =>
        keyword === "" ||
        user.username.toLowerCase().includes(keyword.toLowerCase())
    )
    .map((user) => {
      return (
        <div key={user.id}>
          <UserDetails {...user} ban={ban} remove={remove} />
        </div>
      );
    });

  return (
    <>
      <br />
      <br />
      <div className="search-users">
        <InputGroup className="search-bar">
          <FormControl
            placeholder="Username..."
            aria-label="Username..."
            aria-describedby="basic-addon2"
            value={keyword}
            style={{ maxWidth: "400px" }}
            onChange={(e) => setKeyword(e.target.value)}
          />
          <Button
            variant="outline-dark"
            id="button-addon2"
            onClick={() => setKeyword("")}
          >
            Clear
          </Button>
        </InputGroup>
      </div>
      <br />
      <div>{allUsers}</div>
    </>
  );
};

export default Admin;
