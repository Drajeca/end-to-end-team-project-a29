import React, { useContext, useEffect, useState } from "react";
import { Button, Card, Modal } from "react-bootstrap";
import { Link } from "react-router-dom";
import AuthContext from "../../providers/AuthContext";
import "./CommentDetails.css";
import { TiArrowUpOutline } from "react-icons/ti";
import { AiOutlineDelete } from "react-icons/ai";
import { BsGear } from "react-icons/bs";
import { dateDifference } from "../../common/helper-funtions";
import { GiCheckMark, GiCrossMark } from "react-icons/gi";

const CommentDetails = (props) => {
  const [disabled, setDisabled] = useState(false);
  const [show, setShow] = useState(false);

  const dateCreated = dateDifference(props.createdOn);
  const { user } = useContext(AuthContext);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  useEffect(() => {
    if (user) {
      const result = props.likeStatus.some(
        (item) => item.commentId === props.id && item.userId === user.sub
      );
      setDisabled(result);
    } else {
      return;
    }
  }, [props.id, props.likeStatus, user]);

  return (
    <>
      <div className="comment-details">
        <Card border="dark" style={{ width: "25rem" }} bg="light">
          <Card.Header>{`${props.nickname}: ${
            dateCreated === 0
              ? "Today"
              : dateCreated === 1
              ? `${dateCreated} day ago`
              : `${dateCreated} days ago`
          }`}</Card.Header>
          <Card.Body>
            <Card.Text>{props.content}</Card.Text>
            <Button
              variant="outline-dark"
              disabled={!user || disabled}
              onClick={() => {
                props.like(props.postId, props.id);
                setDisabled(!disabled);
              }}
            >
              <TiArrowUpOutline />
              {`${props.likes}`}
            </Button>
            &nbsp;
            {(user?.sub === props.userId && !props.post.isLocked) ||
            user?.role === "Admin" ? (
              <span>
                <Link to={`/posts/${props.postId}/comments/${props.id}/edit`}>
                  <Button variant="outline-dark">
                    <BsGear />
                  </Button>
                </Link>
                &nbsp;
                <Button variant="outline-dark" onClick={handleShow}>
                  <AiOutlineDelete />
                </Button>
                &nbsp;
              </span>
            ) : null}
          </Card.Body>
        </Card>
        <br />
      </div>

      <Modal
        show={show}
        onHide={handleClose}
        backdrop="static"
        keyboard={false}
      >
        <Modal.Body>Are you sure you want to delete this comment?</Modal.Body>
        <Modal.Footer>
          <Button
            variant="outline-dark"
            onClick={() => props.deleteComment(props.postId, props.id)}
          >
            <GiCheckMark />
          </Button>
          <Button variant="outline-dark" onClick={handleClose}>
            <GiCrossMark />
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default CommentDetails;
