import React, { useState, useEffect } from "react";
import {
  AllPostsLikedByTheUser,
  deletePostEntry,
  getAllPosts,
  likePost,
  lockPost,
} from "../../requests/posts";
import { Button, InputGroup, FormControl } from "react-bootstrap";
import "./Posts.css";
import { AiOutlineClear } from "react-icons/ai";
import IndividualPost from "../IndividualPost/IndividualPost";

const Posts = () => {
  const [posts, setPosts] = useState([]);
  const [error, setError] = useState(null);
  const [keyword, setKeyword] = useState("");
  const [likes, setLikes] = useState([]);

  useEffect(() => {
    AllPostsLikedByTheUser().then((data) => setLikes(data));
  }, []);

  useEffect(() => {
    getAllPosts()
      .then((data) => setPosts(data))

      .catch((error) => setError(error.message));
  }, []);

  const deletePost = (id) => {
    deletePostEntry(id).then((_) => {
      const resultPosts = posts.filter((p) => p.id !== id);
      setPosts(resultPosts);
    });
  };

  const like = (id) => {
    likePost(id).then((result) => {
      if (result.message) {
        return null;
      }
      const postToLike = posts.findIndex((item) => item.id === result.id);
      posts[postToLike] = result;
      setPosts([...posts]);
    });
  };

  const lock = (postId, lockStatus) => {
    lockPost(postId, lockStatus).then((result) => {
      const postToLock = posts.findIndex((item) => item.id === result.id);
      posts[postToLock] = result;
      setPosts([...posts]);
    });
  };

  if (error) {
    return (
      <h4>
        <i>An error has occured: </i>
        {error}
      </h4>
    );
  }

  const allPosts = posts
    .filter(
      (post) =>
        keyword === "" ||
        post.title.toLowerCase().includes(keyword.toLowerCase())
    )
    .map((post) => {
      return (
        <div key={post.id}>
          <IndividualPost
            {...post}
            like={like}
            lock={lock}
            deletePost={deletePost}
            likeState={likes}
          />
        </div>
      );
    })
    .reverse();

  return (
    <main className="all-posts">
      <br />
      <InputGroup className="search-bar">
        <FormControl
          placeholder="Post title here..."
          aria-label="Post title here..."
          aria-describedby="basic-addon2"
          value={keyword}
          style={{ maxWidth: "400px" }}
          onChange={(e) => setKeyword(e.target.value)}
        />
        <Button
          variant="outline-dark"
          id="button-addon2"
          onClick={() => setKeyword("")}
        >
          <AiOutlineClear />
        </Button>
      </InputGroup>
      <br />
      <br />
      {posts.length ? (
        <ul>{allPosts}</ul>
      ) : (
        <div>There are no posts to show</div>
      )}
    </main>
  );
};

export default Posts;
