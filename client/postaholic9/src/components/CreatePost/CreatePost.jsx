import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import "./CreatePost.css";
import { createPost } from "../../requests/posts";
import { Button, Form } from "react-bootstrap";
import { GiCheckMark, GiCrossMark } from "react-icons/gi";

const CreatePost = () => {
  const history = useHistory();
  const [post, setPost] = useState({
    title: "",
    content: "",
  });

  const create = (postData) => {
    if (!postData.title || !postData.content) {
      return alert("Please provide title/content");
    }
    createPost(postData).then((r) =>
      r.message ? alert(r.message) : history.push("/posts")
    );
  };

  const cancel = () => history.push("/posts");

  return (
    <>
      <br />
      <br />
      <Form className="create-post-form">
        <h1 className="create-post-text">Create post</h1>
        <br />
        <Form.Group className="mb-3" controlId="ControlInput1">
          <Form.Label>Title</Form.Label>
          <Form.Control
            type="text"
            placeholder="Title here..."
            value={post.title}
            onChange={(e) => setPost({ ...post, title: e.target.value })}
          />
        </Form.Group>
        <Form.Group className="mb-3" controlId="ControlTextarea1">
          <Form.Label>Content</Form.Label>
          <Form.Control
            as="textarea"
            placeholder="Content here..."
            value={post.content}
            onChange={(e) => setPost({ ...post, content: e.target.value })}
          />
          &nbsp;
          <div>
            <Button
              className="button"
              variant="outline-dark"
              disabled={!post.title || !post.content}
              onClick={() => create(post)}
            >
              <GiCheckMark/>
            </Button>
            &nbsp;&nbsp;
            <Button className="button" variant="outline-dark" onClick={cancel}>
              <GiCrossMark/>
            </Button>
          </div>
        </Form.Group>
      </Form>
    </>
  );
};

export default CreatePost;
