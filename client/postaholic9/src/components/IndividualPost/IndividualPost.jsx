import { Button, Card, Modal } from "react-bootstrap"
import { statusLocked, statusUnlocked } from "../../common/constants"
import { Link } from "react-router-dom";
import { GoComment } from "react-icons/go";
import AuthContext from "../../providers/AuthContext";
import { TiArrowUpOutline } from "react-icons/ti";
import { BsGear, BsLock, BsUnlock } from "react-icons/bs";
import { AiOutlineDelete } from "react-icons/ai";
import { useContext } from "react";
import { useState } from "react";
import { useEffect } from "react";
import { dateDifference } from "../../common/helper-funtions";
import { GiCheckMark, GiCrossMark } from "react-icons/gi";



const IndividualPost = (props) => {
  const [disabled, setDisabled] = useState(false);
  const [show, setShow] = useState(false);

  const { user } = useContext(AuthContext);
  const dateCreated = dateDifference(props.createdOn);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  useEffect(() => {
    if (user) {
      const result = props.likeState.some(
        (item) => item.postId === props.id && item.userId === user.sub
      );
      setDisabled(result);
    } else {
      return;
    }
  }, [props.id, props.likeState, user]);

  return (
    <>
      <div className="individual-post" key={props.id}>
        <Card border="dark" style={{ width: "25rem" }} bg="light">
          <Card.Header>
            {`${props.nickname}: ${
              dateCreated === 0
                ? "Today"
                : dateCreated === 1
                ? `${dateCreated} day ago`
                : `${dateCreated} days ago`
            }`}
            {props.isLocked === statusLocked ? (
              <>
                &nbsp;<b style={{ color: "purple" }}>Locked</b>
              </>
            ) : null}
          </Card.Header>
          <Card.Body>
            <Card.Title>{props.title}</Card.Title>
            <Card.Text>{props.content}</Card.Text>
            <Link to={`/posts/${props.id}/comments`}>
              <Button variant="outline-dark">
                <GoComment />
              </Button>
            </Link>
            &nbsp;
            {props.isLocked === statusLocked &&
            user?.role !== "Admin" ? null : (
              <>
                <Button
                  variant="outline-dark"
                  className="button-like"
                  disabled={!user || disabled}
                  onClick={() => {
                    props.like(props.id);
                    setDisabled(!disabled);
                  }}
                >
                  <TiArrowUpOutline />
                  {`${props.likes}`}
                </Button>
                &nbsp;
              </>
            )}
            {(user?.sub === props.userId &&
              props.isLocked === statusUnlocked) ||
            user?.role === "Admin" ? (
              <span>
                <Link to={`/posts/${props.id}/edit`}>
                  <Button variant="outline-dark">
                    <BsGear />
                  </Button>
                </Link>
                &nbsp;
                <Button variant="outline-dark" onClick={handleShow}>
                  <AiOutlineDelete />
                </Button>
                &nbsp;
              </span>
            ) : null}
            {user?.role === "Admin" ? (
              props.isLocked === statusUnlocked ? (
                <Button
                  variant="outline-dark"
                  onClick={() => props.lock(props.id, statusLocked)}
                >
                  <BsLock />
                </Button>
              ) : (
                <Button
                  variant="outline-dark"
                  onClick={() => props.lock(props.id, statusUnlocked)}
                >
                  <BsUnlock />
                </Button>
              )
            ) : null}
            <br />
          </Card.Body>
        </Card>
        <br />
      </div>

      <Modal
        show={show}
        onHide={handleClose}
        backdrop="static"
        keyboard={false}
      >
        <Modal.Body>Are you sure you want to delete this post?</Modal.Body>
        <Modal.Footer>
          <Button
            variant="outline-dark"
            onClick={() => props.deletePost(props.id)}
          >
            <GiCheckMark/>
          </Button>
          <Button variant="outline-dark" onClick={handleClose}>
            <GiCrossMark />
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default IndividualPost;