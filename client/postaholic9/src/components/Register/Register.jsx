import { useState } from "react";
import { Form, Button } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import { register } from "../../requests/authorization";
import { registerValidator } from "../../validators/register-validations";
import "./Register.css";

export const Register = () => {
  const [isFormValid, setIsFormValid] = useState(false);

  const [credentials, setCredentials] = useState({
    username: {
      value: "",
      type: "text",
      placeholder: "Set username...",
      valid: true,
      touched: false,
      fieldReq: "Between 3 and 20 symbols, including letters and digits",
    },

    nickname: {
      value: "",
      type: "text",
      placeholder: "Set display name...",
      valid: true,
      touched: false,
      fieldReq: "Between 3 and 20 symbols, including letters and digits",
    },

    password: {
      value: "",
      type: "password",
      placeholder: "Set password...",
      valid: true,
      touched: false,
      fieldReq:
        "Between 4 and 10 characters, at least one letter and one number:",
    },
  });

  const history = useHistory();

  const handleInputChange = (e) => {
    const { name, value } = e.target;

    const updatedInput = credentials[name];
    updatedInput.value = value;

    updatedInput.valid = registerValidator[name](value);
    updatedInput.touched = true;

    setCredentials({ ...credentials, [name]: updatedInput });

    const formValid = Object.values(credentials).every(
      (element) => element.valid && element.touched
    );
    setIsFormValid(formValid);
  };

  const triggerRegister = (e) => {
    e.preventDefault();

    if (!isFormValid) {
      console.log("form invalid");
      return;
    }

    if (
      !credentials.username.value ||
      !credentials.password.value ||
      !credentials.nickname.value
    ) {
      return alert("Invalid credentials...");
    }
    register(
      credentials.username.value,
      credentials.password.value,
      credentials.nickname.value
    ).then((error) =>
      error.message ? alert(error.message) : history.push("/login")
    );
  };

  const inputFields = Object.entries(credentials).map(([key, element]) => {
    return (
      <Form.Group className="mb-3" key={key}>
        <Form.Control
          style={element.valid ? null : { border: "2px solid red" }}
          name={key}
          type={element.type}
          placeholder={element.placeholder}
          value={element.value}
          onChange={handleInputChange}
        />
        <div style={{ color: "gray" }}>{element.fieldReq}</div>
        <br />
      </Form.Group>
    );
  });

  return (
    <Form className="register-form" onSubmit={triggerRegister}>
      <h1 className="register-text">Register</h1>
      <br />
      {inputFields}
      <Button type="submit" variant="outline-dark" disabled={!isFormValid}>
        Register
      </Button>
    </Form>
  );
};
