import React, { useContext } from "react";
import { NavLink, useHistory, withRouter } from "react-router-dom";
import AuthContext from "../../providers/AuthContext";
import { Nav, Navbar, Container, Button } from "react-bootstrap";
import "./Header.css";

const Header = (props) => {
  const history = useHistory();
  const { user, setUser } = useContext(AuthContext);
  const triggerLogout = () => {
    setUser(null);
    localStorage.removeItem("token");
    history.push("/home");
  };
  const { location } = props;

  return (
    <Navbar bg="dark" variant="dark">
      <Container>
        <Navbar.Brand as={NavLink} exact to="/home" className="logo-text">
          Post(a)holic9
        </Navbar.Brand>
        <Nav activeKey={location.pathname} className="ml-auto">
          <Nav.Link as={NavLink} exact to="/home">
            Home
          </Nav.Link>
          <Nav.Link as={NavLink} exact to="/posts">
            Posts
          </Nav.Link>
          <Nav.Link as={NavLink} exact to="/posts/new">
            Create Post
          </Nav.Link>

          {user ? (
            <Nav.Link as={NavLink} exact to="/posts/user">
              My Posts
            </Nav.Link>
          ) : null}

          <Nav.Link as={NavLink} exact to="/about">
            About
          </Nav.Link>

          {user && user.role === "Admin" ? (
            <Nav.Link as={NavLink} exact to="/users/admin">
              Admin
            </Nav.Link>
          ) : null}

          {user ? (
            <>
              <Button variant="outline-light" onClick={triggerLogout}>
                Logout
              </Button>
              &nbsp;&nbsp;
            </>
          ) : (
            <Nav.Link as={NavLink} exact to="/login">
              <Button variant="outline-light">Login</Button>{" "}
            </Nav.Link>
          )}
          {user ? (
            <Navbar.Text>Signed in as: {`${user?.username}`}</Navbar.Text>
          ) : null}
        </Nav>
      </Container>
    </Navbar>
  );
};

export default withRouter(Header);
