import React from "react";
import { Card } from "react-bootstrap";
import { dateDifference } from "../../common/helper-funtions";

const PostDetails = (props) => {
  const dateCreated = dateDifference(props.createdOn);

  return (
    <div className="individual-post">
      <Card border="dark" style={{ width: "25rem" }} bg="light">
        <Card.Header>{`${props.nickname}: ${
          dateCreated === 0
            ? "Today"
            : dateCreated === 1
            ? `${dateCreated} day ago`
            : `${dateCreated} days ago`
        }`}</Card.Header>
        <Card.Body>
          <Card.Title>{props.title}</Card.Title>
          <Card.Text>{props.content}</Card.Text>
        </Card.Body>
      </Card>
      <br />
    </div>
  );
};

export default PostDetails;
