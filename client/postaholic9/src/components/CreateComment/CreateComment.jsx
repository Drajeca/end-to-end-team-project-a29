import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import "./CreateComment.css";
import { createComment } from "../../requests/comments";
import { Button, Form } from "react-bootstrap";
import { GiCheckMark, GiCrossMark } from "react-icons/gi";

const CreateComment = (props) => {
  const history = useHistory();
  const [comment, setComment] = useState({ content: "" });

  const id = props.match.params.id;

  const create = (commentData) => {
    //create new comment
    createComment(commentData, id).then((_) =>
      history.push(`/posts/${id}/comments`)
    );
  };
  const cancel = () => history.push(`/posts/${id}/comments`);

  return (
    <>
      <br />
      <br />
      <Form className="create-comment-form">
        <h1 className="create-comment-text">Create comment</h1>
        <br />
        <Form.Group className="mb-3" controlId="ControlTextarea1">
          <Form.Label>Content</Form.Label>
          <Form.Control
            as="textarea"
            placeholder="Content here..."
            value={comment.content}
            onChange={(e) =>
              setComment({ ...comment, content: e.target.value })
            }
          />
          &nbsp;
          <div>
            <Button
              className="button"
              variant="outline-dark"
              disabled={!comment.content}
              onClick={() => create(comment)}
            >
              <GiCheckMark/>
            </Button>
            &nbsp;&nbsp;
            <Button className="button" variant="outline-dark" onClick={cancel}>
              <GiCrossMark/>
            </Button>
          </div>
        </Form.Group>
      </Form>
    </>
  );
};

export default CreateComment;
