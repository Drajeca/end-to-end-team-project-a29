import React, { useEffect, useState } from "react";
import { Form, Button } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import { getPostById, updatePostEntry } from "../../requests/posts";
import { GiCheckMark, GiCrossMark } from "react-icons/gi";
import "./UpdatePost.css";

const UpdatePost = (props) => {
  const id = props.match.params.id;
  const history = useHistory();

  const [updatePost, setUpdatePost] = useState({
    title: "",
    content: "",
  });

  const [error, setError] = useState("");

  useEffect(() => {
    getPostById(id)
      .then((data) => setUpdatePost(data))
      .catch((error) => setError(error.message));
  }, [id]);

  const update = (postData) => {
    updatePostEntry(postData, id).then((_) => history.goBack());
  };

  const cancel = () => history.goBack();

  if (error) {
    return <h4><i>An error has occurred: </i>{error}</h4>
  }

  return (
    <>
      <br />
      <h4 className="update-post-text">Update post</h4>
      <Form className="create-post-form">
        <Form.Group className="mb-3" controlId="ControlInput1">
          <Form.Label>Title</Form.Label>
          <Form.Control
            type="text"
            value={updatePost.title}
            onChange={(e) =>
              setUpdatePost({ ...updatePost, title: e.target.value })
            }
          />
        </Form.Group>
        <Form.Group className="mb-3" controlId="ControlTextarea1">
          <Form.Label>Content</Form.Label>
          <Form.Control
            as="textarea"
            value={updatePost.content}
            onChange={(e) =>
              setUpdatePost({ ...updatePost, content: e.target.value })
            }
          />
          &nbsp;
          <div>
            <Button
              className="button"
              variant="outline-dark"
              onClick={() => update(updatePost)}
            >
              <GiCheckMark />
            </Button>
            &nbsp;&nbsp;
            <Button className="button" variant="outline-dark" onClick={cancel}>
              <GiCrossMark />
            </Button>
          </div>
        </Form.Group>
      </Form>
    </>
  );
};

export default UpdatePost;
