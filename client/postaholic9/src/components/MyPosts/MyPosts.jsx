import React, { useState, useEffect } from "react";
import {
  AllPostsLikedByTheUser,
  deletePostEntry,
  getUserPosts,
  likePost,
  lockPost,
} from "../../requests/posts";
import IndividualPost from "../IndividualPost/IndividualPost";
import "./MyPosts.css";

const MyPosts = () => {
  const [posts, setPosts] = useState([]);
  const [error, setError] = useState(null);
  const [likes, setLikes] = useState([]);

  useEffect(() => {
    AllPostsLikedByTheUser().then((data) => setLikes(data));
  }, []);

  useEffect(() => {
    getUserPosts()
      .then((data) => setPosts(data))

      .catch((error) => setError(error.message));
  }, []);

  const deletePost = (id) => {
    deletePostEntry(id).then((_) => {
      const resultPosts = posts.filter((p) => p.id !== id);
      setPosts(resultPosts);
    });
  };

  const like = (id) => {
    likePost(id).then((result) => {
      if (result.message) {
        return null;
      }
      const postToLike = posts.findIndex((item) => item.id === result.id);
      posts[postToLike] = result;
      setPosts([...posts]);
    });
  };

  const toggleLikeButton = (postId, userId) => {
    return likes.some(
      (item) => item.postId === postId && item.userId === userId
    );
  };
  const lock = (postId, lockStatus) => {
    lockPost(postId, lockStatus).then((result) => {
      const postToLock = posts.findIndex((item) => item.id === result.id);
      posts[postToLock] = result;
      setPosts([...posts]);
    });
  };

  if (error) {
    return (
      <h4>
        <i>An error has occurred: </i>
        {error}
      </h4>
    );
  }

  const allPosts = posts
    .map((post) => {
      return (
        <div className="individual-post" key={post.id}>
          {
            <IndividualPost
              {...post}
              like={like}
              lock={lock}
              deletePost={deletePost}
              toggleLikeButton={toggleLikeButton}
              likeState={likes}
            />
          }
        </div>
      );
    })
    .reverse();

  return (
    <main className="all-user-posts">
      <br />
      <h2 className="my-posts-text">My Posts</h2>
      <br />
      <br />
      {posts.length ? <ul>{allPosts}</ul> : <div>You have no posts yet</div>}
    </main>
  );
};

export default MyPosts;
