import React, { useEffect, useState } from "react";
import { Button, Form } from "react-bootstrap";
import { GiCheckMark, GiCrossMark } from "react-icons/gi";
import { useHistory } from "react-router-dom";
import { getCommentById, updateCommentEntry } from "../../requests/comments";
import "./UpdateComment.css";

const UpdateComment = (props) => {
  const { postId, commentId } = props.match.params;
  const [updateComment, setUpdateComment] = useState({ content: "" });
  const [error, setError] = useState("");
  const history = useHistory();

  //get the comment and set it as an initial update state
  useEffect(() => {
    getCommentById(postId, commentId)
      .then((data) => setUpdateComment(data))
      .catch((error) => setError(error.message));
  }, [postId, commentId]);

  //actual update of the comment
  const update = (commentData) => {
    updateCommentEntry(postId, commentId, commentData).then((_) =>
      history.push(`/posts/${postId}/comments`)
    );
  };

  if (error) {
    return <h4><i>An error has ocurred: </i>{error}</h4>
  }

  const cancel = () => history.push(`/posts/${postId}/comments`);

  return (
    <>
      <br />
      <h4 className="update-comment-text">Update comment</h4>
      <Form className="create-comment-form">
        <Form.Group className="mb-3" controlId="ControlTextarea1">
          <Form.Label>Content</Form.Label>
          <Form.Control
            as="textarea"
            placeholder="Content here..."
            value={updateComment.content}
            onChange={(e) =>
              setUpdateComment({ ...updateComment, content: e.target.value })
            }
          />
          &nbsp;
          <div>
            <Button
              className="button"
              variant="outline-dark"
              onClick={() => update(updateComment)}
            >
              <GiCheckMark />
            </Button>
            &nbsp;&nbsp;
            <Button className="button" variant="outline-dark" onClick={cancel}>
              <GiCrossMark />
            </Button>
          </div>
        </Form.Group>
      </Form>
    </>
  );
};

export default UpdateComment;
