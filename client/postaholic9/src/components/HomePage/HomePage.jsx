import React from "react";
import { Carousel } from "react-bootstrap";
import "./HomePage.css";
import { Link } from "react-router-dom";

const HomePage = () => {
  return (
    <div className="home-page">
      <Carousel variant="light">
        <Carousel.Item style={{ height: "91vh" }}>
          <img
            className="d-block w-100 h-100"
            src="https://ak.picdn.net/shutterstock/videos/1008865982/thumb/1.jpg"
            alt="First slide"
          />
          <Carousel.Caption>
            <h5>Welcome To Post(a)holic9</h5>
            <p>Sometimes You Feel Like A Postaholic, Sometimes You Don't.</p>
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item style={{ height: "91vh" }}>
          <img
            className="d-block w-100 h-100"
            src="https://i.pinimg.com/originals/5d/59/b6/5d59b6a040d9df5da65cce6257cee97c.jpg"
            alt="Second slide"
          />
          <Carousel.Caption>
            <h5>Already have an account?</h5>
            <p>
              <Link to="/login">Sign in here!</Link>
            </p>
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item style={{ height: "91vh" }}>
          <img
            className="d-block w-100 h-100"
            src="https://wallpaperaccess.com/full/334700.jpg"
            alt="Third slide"
          />
          <Carousel.Caption>
            <h5>Not sure where to start?</h5>
            <p>
              <Link to="/login/register">Register for free.</Link>
            </p>
          </Carousel.Caption>
        </Carousel.Item>
      </Carousel>
    </div>
  );
};

export default HomePage;
