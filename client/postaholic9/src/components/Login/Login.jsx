import { useContext, useState } from "react";
import { Button, Form } from "react-bootstrap";
import { Link, useHistory } from "react-router-dom";
import AuthContext from "../../providers/AuthContext";
import { signIn } from "../../requests/authorization";
import { getUserFromToken } from "../../utils/token";
import "./Login.css";

export const Login = () => {
  const { setUser } = useContext(AuthContext);
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const history = useHistory();

  const triggerLogin = () => {
    if (!username || !password) {
      return alert("Invalid username/password!");
    }

    signIn(username, password)
      .then((data) => {
        if (data.message) {
          return alert(data.message);
        }

        setUser(getUserFromToken(data.token));
        localStorage.setItem("token", data.token);
      })
      .then((_) => localStorage.getItem("token") && history.push("/posts"))
      .catch((error) => console.error(error));
  };
  return (
    <Form className="login-form">
      <h1 className="login-text">Login</h1>
      <br />
      <Form.Group className="mb-3" controlId="ControlInput1">
        <Form.Control
          className="login-bar"
          type="text"
          placeholder="Username"
          value={username}
          onChange={(e) => setUsername(e.target.value)}
        />
      </Form.Group>
      <Form.Group className="mb-3" controlId="ControlTextarea1">
        <Form.Control
          type="password"
          placeholder="******"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
        />
        &nbsp;
      </Form.Group>
      <Button className="button" variant="outline-dark" onClick={triggerLogin}>
        Login
      </Button>
      &nbsp;&nbsp;
      <span>Don't have an account?&nbsp;</span>
      <Link to={"/login/register"}>Click here!</Link>
    </Form>
  );
};

export default Login;
