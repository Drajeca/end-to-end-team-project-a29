import { Button, Form, Modal } from "react-bootstrap";
import { useState } from "react"
import { Card } from "react-bootstrap";
import { IoSkullOutline } from 'react-icons/io5'
import { ImHappy } from 'react-icons/im'
import { AiOutlineDelete } from 'react-icons/ai'
import './UserDetails.css'
import { GiCheckMark, GiCrossMark } from "react-icons/gi";

const UserDetails = (props) => {
  const [duration, setDuration] = useState("");
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  return (
    <>
      <div className="individual-user">
        <Card border="dark" style={{ width: "25rem" }} bg="light">
          <Card.Header>
            {`Username: ${props.username}`}{" "}
            <Button variant="outline-dark" onClick={handleShow}>
              <AiOutlineDelete />
            </Button>
          </Card.Header>
          <Card.Body>
            <Card.Title>{`Display name: ${props.nickname}`}</Card.Title>
            <Card.Text>
              {`Created on: ${new Date(props.createdOn).toLocaleDateString()}`}
              <br />
              {props.isBanned ? (
                <Form.Label>{`Banned until: ${new Date(
                  props.isBanned
                ).toLocaleDateString()}`}</Form.Label>
              ) : null}
            </Card.Text>
            <Form.Group controlId="ban">
              <Form.Label>Set ban duration:</Form.Label>
              <Form.Control
                type="date"
                name="ban"
                placeholder="Ban duration"
                value={duration}
                onChange={(e) => setDuration(e.target.value)}
              />
            </Form.Group>
            <br />
            <Button
              variant="outline-dark"
              onClick={() => props.ban(props.id, duration)}
            >
              <IoSkullOutline />
            </Button>
            &nbsp;
            <Button
              variant="outline-dark"
              onClick={() => props.ban(props.id, new Date())}
            >
              <ImHappy />
            </Button>
            <br />
          </Card.Body>
        </Card>
        <br />
      </div>

      <Modal
        show={show}
        onHide={handleClose}
        backdrop="static"
        keyboard={false}
      >
        <Modal.Body>{`Are you sure you want to delete user ${props.username} ?`}</Modal.Body>
        <Modal.Footer>
          <Button variant="outline-dark" onClick={() => props.remove(props.id)}>
            <GiCheckMark />
          </Button>
          <Button variant="outline-dark" onClick={handleClose}>
            <GiCrossMark />
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default UserDetails;