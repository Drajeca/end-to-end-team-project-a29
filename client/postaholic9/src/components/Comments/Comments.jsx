import React, { useEffect, useState } from "react";
import { Button } from "react-bootstrap";
import { Link } from "react-router-dom";
import {
  AllCommentsLikedByTheUser,
  deleteCommentEntry,
  getAllPostComments,
  likeComment,
} from "../../requests/comments";
import { getPostById } from "../../requests/posts";
import CommentDetails from "../CommentDetails/CommentDetails";
import PostDetails from "../PostDetails/PostDetails";
import { IoCreateOutline } from "react-icons/io5";
import "./Comments.css";
import { statusLocked } from "../../common/constants";
import { useContext } from "react";
import AuthContext from "../../providers/AuthContext";

const Comments = (props) => {
  const [post, setPost] = useState(null);
  const [comments, setComments] = useState([]);
  const [error, setError] = useState(null);
  const { user } = useContext(AuthContext);
  const [likes, setLikes] = useState([]);

  const id = props.match.params.id;
  useEffect(() => {
    AllCommentsLikedByTheUser()
    .then((data) => setLikes(data))
    .catch(error => setError(error.message));
  }, []);

  //get the post
  useEffect(() => {
    getPostById(id)
      .then((data) => setPost(data))
      .catch((error) => setError(error.message));
  }, [id]);

  //get all post comments
  useEffect(() => {
    getAllPostComments(id)
      .then((data) => setComments(data))
      .catch((error) => setError(error.message));
  }, [id]);

  //handle delete comment
  const deleteComment = (id, commentId) => {
    deleteCommentEntry(id, commentId).then((_) => {
      const resultComments = comments.filter((c) => c.id !== commentId);
      setComments(resultComments);
    });
  };

  //like comment
  const like = (postId, commentId) => {
    likeComment(postId, commentId).then((result) => {
      if (result.message) {
        return null;
      }
      const commentToLike = comments.findIndex((item) => item.id === commentId);
      comments[commentToLike] = result;
      setComments([...comments]);
    });
  };
  
  if (post === null) {
    return <div>Loading</div>;
  }
  
  if (post.message) {
    return <div>Post not found</div>;
  }

  if (error && user) {
    return (
      <h4>
        <i>An error has occurred: </i>
        {error}
      </h4>
    );
  }

  const allComments = comments.length ? (
    comments
      .map((c) => {
        return (
          <div className="single-comment" key={c.id}>
            <CommentDetails
              {...c}
              post={post}
              postId={id}
              deleteComment={deleteComment}
              like={like}
              likeStatus={likes}
            />
          </div>
        );
      })
      .reverse()
  ) : (
    <div className="no-comments-text">Be the first one to comment...</div>
  );

  return (
    <main className="comments-post">
      <br />
      <PostDetails {...post} />
      {post.isLocked === statusLocked && user?.role !== "Admin" ? (
        <h4 className="locked-warning" style={{ color: "purple" }}>
          Post is locked
        </h4>
      ) : (
        <Link to={`/posts/${id}/comments/new`} className="write-comment-button">
          <Button variant="outline-dark">
            Write a comment <IoCreateOutline />
          </Button>
        </Link>
      )}
      <br />
      <h2 className="all-comments-text">Comments</h2>
      <br />
      <br />
      {allComments}
    </main>
  );
};

export default Comments;
