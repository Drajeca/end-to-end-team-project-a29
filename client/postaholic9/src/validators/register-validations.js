export const registerValidator = {
  username: (value) => /(^[a-zA-Z\d]{3,20}$)/.test(value),
  nickname: (value) => /(^[a-zA-z\d]{3,20}$)/.test(value),
  password: (value) => /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{4,10}$/.test(value),
};
