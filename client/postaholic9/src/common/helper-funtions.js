export const dateDifference = (date) => {
  const today = new Date();
  const before = new Date(date);
  const todayUtc = Date.UTC(
    today.getFullYear(),
    today.getMonth(),
    today.getDate()
  );
  const beforeUtc = Date.UTC(
    before.getFullYear(),
    before.getMonth(),
    before.getDate()
  );
  const day = 1000 * 60 * 60 * 24;
  return (todayUtc - beforeUtc) / day;
};
