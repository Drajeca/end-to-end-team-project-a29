export const BASE_URL = 'http://localhost:5001';

export const TOKEN = localStorage.getItem('token');

export const statusLocked = 1;
export const statusUnlocked = 0;