import { BASE_URL } from "../common/constants";

export const signIn = (username, password) =>
  fetch(`${BASE_URL}/auth/signin`, {
    method: "POST",
    headers: {
      "Content-Type": "Application/json",
    },
    body: JSON.stringify({
      username,
      password,
    }),
  }).then((r) => r.json());

export const register = (username, password, nickname) =>
  fetch(`${BASE_URL}/users`, {
    method: "POST",
    headers: {
      "Content-Type": "Application/json",
    },
    body: JSON.stringify({
      username,
      password,
      nickname,
    }),
  }).then((r) => r.json());
