import { BASE_URL } from "../common/constants";

export const getAllPostComments = (postId) =>
  fetch(`${BASE_URL}/posts/${postId}/comments`).then((r) => r.json());

export const deleteCommentEntry = (postId, commentId) =>
  fetch(`${BASE_URL}/posts/${postId}/comments/${commentId}`, {
    method: "DELETE",
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
      "Content-Type": "Application/json",
    },
  }).then((r) => r.json());

export const createComment = (commentData, postId) =>
  fetch(`${BASE_URL}/posts/${postId}/comments`, {
    method: "POST",
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
      "Content-Type": "Application/json",
    },
    body: JSON.stringify(commentData),
  }).then((r) => r.json());

export const getCommentById = (postId, commentId) =>
  fetch(`${BASE_URL}/posts/${postId}/comments/${commentId}`).then((r) =>
    r.json()
  );

export const updateCommentEntry = (postId, commentId, commentData) =>
  fetch(`${BASE_URL}/posts/${postId}/comments/${commentId}`, {
    method: "PUT",
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
      "Content-Type": "Application/json",
    },
    body: JSON.stringify(commentData),
  }).then((r) => r.json());

export const likeComment = (postId, commentId) =>
  fetch(`${BASE_URL}/posts/${postId}/comments/${commentId}/likes`, {
    method: "POST",
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
      "Content-Type": "Application/json",
    },
  }).then((r) => r.json());

export const AllCommentsLikedByTheUser = () =>
  fetch(`${BASE_URL}/posts/comments/likes/user`, {
    method: "GET",
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
      "Content-Type": "Application/json",
    },
  }).then((r) => r.json());
