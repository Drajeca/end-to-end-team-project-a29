import { BASE_URL } from "../common/constants";

export const getAllUsers = (search = "") =>
  fetch(`${BASE_URL}/users?search=${search}`).then((r) => r.json());

export const banUser = (userId, banDuration) =>
  fetch(`${BASE_URL}/admin/ban/${userId}`, {
    method: "PUT",
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
      "Content-Type": "Application/json",
    },
    body: JSON.stringify({
      duration: banDuration,
    }),
  }).then((r) => r.json());

export const deleteUser = (userId) =>
  fetch(`${BASE_URL}/admin/delete/${userId}`, {
    method: "DELETE",
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
      "Content-Type": "Application/json",
    },
  }).then((r) => r.json());
