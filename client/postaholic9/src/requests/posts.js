import { BASE_URL } from "../common/constants";

export const getPostById = (id) =>
  fetch(`${BASE_URL}/posts/${id}`).then((r) => r.json());

export const getAllPosts = () =>
  fetch(`${BASE_URL}/posts`).then((r) => r.json());

export const getUserPosts = () =>
  fetch(`${BASE_URL}/posts/user/allposts`, {
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
      "Content-Type": "Application/json",
    },
  }).then((r) => r.json());

export const createPost = (postData) =>
  fetch(`${BASE_URL}/posts`, {
    method: "POST",
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
      "Content-Type": "Application/json",
    },
    body: JSON.stringify(postData),
  }).then((r) => r.json());

export const deletePostEntry = (postId) =>
  fetch(`${BASE_URL}/posts/${postId}`, {
    method: "DELETE",
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
      "Content-Type": "Application/json",
    },
  }).then((r) => r.json());

export const updatePostEntry = (postData, postId) =>
  fetch(`${BASE_URL}/posts/${postId}`, {
    method: "PUT",
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
      "Content-Type": "Application/json",
    },
    body: JSON.stringify(postData),
  }).then((r) => r.json());

export const likePost = (postId) =>
  fetch(`${BASE_URL}/posts/${postId}/likes`, {
    method: "POST",
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
      "Content-Type": "Application/json",
    },
  }).then((r) => r.json());

export const numberOfPostLikes = (postId) =>
  fetch(`${BASE_URL}/posts/${postId}/likes`).then((r) => r.json());

export const lockPost = (postId, lockStatus) =>
  fetch(`${BASE_URL}/admin/lock/${postId}`, {
    method: "PUT",
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
      "Content-Type": "Application/json",
    },
    body: JSON.stringify({
      lockStatus: lockStatus,
    }),
  }).then((r) => r.json());

export const getAllPostLikes = (postId) =>
  fetch(`${BASE_URL}/posts/${postId}/likes`).then((r) => r.json());

export const AllPostsLikedByTheUser = () =>
  fetch(`${BASE_URL}/posts/likes/user`, {
    method: "GET",
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
      "Content-Type": "Application/json",
    },
  }).then((r) => r.json());
