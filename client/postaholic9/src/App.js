import './App.css';
import React, { useState } from 'react';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import Header from './components/Header/Header';
import Posts from './components/Posts/Posts';
import HomePage from './components/HomePage/HomePage';
import CreatePost from './components/CreatePost/CreatePost';
import Comments from './components/Comments/Comments';
import CreateComment from './components/CreateComment/CreateComment';
import UpdatePost from './components/UpdatePost/UpdatePost';
import UpdateComment from './components/UpdateComment/UpdateComment';
import AuthContext from './providers/AuthContext';
import Login from './components/Login/Login';
import { getUserFromToken } from './utils/token';
import GuardedRoute from './providers/GuardedRoute';
import { Register } from './components/Register/Register';
import About from './components/About/About';
import MyPosts from './components/MyPosts/MyPosts';
import Admin from './components/Admin/Admin';

function App() {

  const [user, setUser] = useState(getUserFromToken(localStorage.getItem('token')))

  return (
    <BrowserRouter>
      <AuthContext.Provider value={{ user, setUser }}>
       <Header />
        <Switch>
          <Redirect exact path="/" to="/home"/> 
          <Route exact path ="/home" component={HomePage}/>
          <Route exact path ="/about" component={About}/>
          <Route exact path="/posts" component={Posts}/>
          <GuardedRoute exact path="/posts/new" auth={ user !== null } component={CreatePost}/>
          <Route exact path="/posts/:id/comments" component={Comments}/>
          <GuardedRoute exact path="/posts/:id/comments/new" auth={ user !== null } component={CreateComment}/>
          <Route exact path ="/posts/:id/edit" component={UpdatePost} />
          <Route exact path ="/posts/:postId/comments/:commentId/edit" component={UpdateComment} />
          <Route exact path ="/login" component={Login} />
          <Route exact path ="/login/register" component={Register} />
          <Route exact path ="/posts/user" component={MyPosts} />
          <Route exact path ="/users/admin" component={Admin} />
        </Switch>
      </AuthContext.Provider>
    </BrowserRouter>
  );
}

export default App;
